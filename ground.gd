extends MeshInstance

export var bright_color = Color("#aaaaaa") setget set_bright_color
export var dark_color = Color("#408f3a") setget set_dark_color

const GROUND_LEVEL = -0.18
const OFFSET = 0.5
const PADDING = 10

var saved_size_x
var saved_size_z

func set_bright_color(new):
    bright_color = new
    if saved_size_x != null:
        create(saved_size_x, saved_size_z)

func set_dark_color(new):
    dark_color = new
    if saved_size_x != null:
        create(saved_size_x, saved_size_z)

func create(size_x, size_z):
    saved_size_x = size_x
    saved_size_z = size_z

    var st = SurfaceTool.new()
    st.begin(Mesh.PRIMITIVE_TRIANGLES)

    var min_x = -PADDING
    var min_z = -(size_z + PADDING)

    var x_step = 1
    var z_step = 1

    min_x -= x_step / 2.0
    min_z += z_step / 2.0

    st.add_normal(Vector3(0, 1, 0))

    var material = SpatialMaterial.new()
    material.vertex_color_use_as_albedo = true
    st.set_material(material)

    for z in range(size_z + PADDING * 2):
        for x in range(size_x + PADDING * 2):
            var x_pos = min_x + x_step * x
            var z_pos = min_z + z_step * z

            if (x + z) % 2 == 0:
                st.add_color(bright_color)
            else:
                st.add_color(dark_color)

            st.add_vertex(Vector3(x_pos, GROUND_LEVEL, z_pos))
            st.add_vertex(Vector3(x_pos + x_step, GROUND_LEVEL, z_pos + z_step))
            st.add_vertex(Vector3(x_pos, GROUND_LEVEL, z_pos + z_step))

            st.add_vertex(Vector3(x_pos, GROUND_LEVEL, z_pos))
            st.add_vertex(Vector3(x_pos + x_step, GROUND_LEVEL, z_pos))
            st.add_vertex(Vector3(x_pos + x_step, GROUND_LEVEL, z_pos + z_step))

    st.index()


    var mesh = Mesh.new()
    st.commit(mesh)

    self.mesh = mesh

