tool
extends EditorPlugin

const NODE_NAME = "DebugInformation"

func _enter_tree():
    add_custom_type(NODE_NAME,
                    "VBoxContainer",
                    preload("res://addons/debug_information/debug_information.gd"),
                    preload("res://addons/debug_information/icon.svg"))
    
    add_autoload_singleton(NODE_NAME, "res://addons/debug_information/autoload.gd")

func _exit_tree():
    remove_custom_type(NODE_NAME)
    remove_autoload_singleton(NODE_NAME)
