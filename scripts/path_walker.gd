# Custom iterator to walk a path and get all conveyor positions from that
# path. The iterator will include the final junction/stop/goal.

# The difference between a path and route is that a path contains one or more
# routes, so getting a path requires the router, while a route stops at a
# router

const Position = preload("res://scripts/position.gd")
const Map = preload("res://scripts/map.gd")
const Router = preload("res://scripts/router.gd")

const Base = preload("res://scripts/conveyors/base.gd")

# Hungarian notation due to lack of language features :(
var c_map: Map
var c_router: Router
var c_start_position: Position
var c_start_direction: int
var c_path_id: int
# mut
var _position: Position
var _last_direction: int
var _next_direction: int
var _end_found: bool

func _init(map: Map, router: Router, path_id: int, position: Position, direction: int):
    self.c_map = map
    self.c_router = router
    self.c_start_position = Position.new(position.x, position.y)
    self.c_start_direction = direction
    self.c_path_id = path_id

    _iter_init(self)

func _iter_init(_self) -> bool:
    self._position = Position.new(c_start_position.x, c_start_position.y)
    self._next_direction = _get_outgoing_direction(_position, c_start_direction)
    self._last_direction = Direction.NONE
    self._end_found = false
    return c_map.try_get_at(_position) != null

func _iter_next(_self) -> bool:
    # When a stop, spawner, etc is found it should be included in the iterator,
    # so the method must return true. After that it should return false.
    # _end_found makes sure this is what happens

    if _end_found:
        return false

    _position.move_in_direction(_next_direction)

    var conveyor := c_map.try_get_at(_position)
    # This case is possible if there is no conveyor at _start_position +
    # next_direction
    if conveyor == null:
        return false

    if Direction.count_directions(conveyor.get_directions()) == 1:
        _end_found = true
    else:
        _last_direction = _next_direction
        _next_direction = _get_outgoing_direction(_position, _next_direction)

    return true

func _iter_get(_self) -> Position:
    return Position.new(_position.x, _position.y)

# get is reserved
func current() -> Position:
    return Position.new(_position.x, _position.y)

func _get_outgoing_direction(position: Position, current_direction: int) -> int:
    var conveyor := c_map.try_get_at(position)
    if conveyor == null:
        return Direction.NONE

    if conveyor.is_junction():
        # the direction should have been set earlier since the box is now on
        # this junction
        var next_outgoing_direction := c_router.get_direction(c_path_id, position.x, position.y)
        if next_outgoing_direction != Direction.NONE:
            return next_outgoing_direction

        var directions := conveyor.get_directions()
        if Direction.includes(directions, current_direction):
            return current_direction
        else:
            var route_rotation := [ Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT ]
            # If the cargo is at a junction where it cannot continue in the
            # direction it is currently travelling, that means any of the
            # other three directions work.
            var new_route_index := (route_rotation.find(current_direction) + 1) % route_rotation.size()
            return route_rotation[new_route_index]
    else:
        var directions := conveyor.get_directions()
        return directions & ~(directions & Direction.flip_direction(current_direction))

func skip() -> Object:
    _iter_next(self)
    return self

func conveyor() -> Base:
    return c_map.try_get_at(_position)

func last_direction() -> int:
    return _last_direction

func next_direction() -> int:
    return _next_direction
