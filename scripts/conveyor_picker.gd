extends HBoxContainer

signal button_toggled

const PreviewButton = preload("res://preview_button.tscn")

var conveyor_types: = []

func _ready():
    connect("hide", self, "_on_Hidden")

func add_button(path: String, name, label: String) -> void:
    var conveyor_button: Node = PreviewButton.instance().init(path)
    conveyor_button.connect("button_toggled", self, "child_pressed", [conveyor_types.size()])
    (conveyor_button.find_node("Label") as Label).text = label
    add_child(conveyor_button)
    conveyor_types.append(name)

var changing = false
func child_pressed(value, index) -> void:
    # Calling set_pressed also triggers the button_toggled signal, which means
    # this function will be called again. Any nested call should be ignored
    if changing:
        return

    changing = true
    var children = get_children()
    for i in range(get_child_count()):
        if i != index:
            var child = children[i].find_node("Button")
            if child.is_pressed():
                child.set_pressed(false)
    emit_signal("button_toggled", conveyor_types[index], value)
    changing = false

func _on_Hidden():
    var children = get_children()
    for i in range(get_child_count()):
        var child = children[i].find_node("Button")
        if child.is_pressed():
            child.set_pressed(false)
