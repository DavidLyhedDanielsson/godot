#var PHYSICS_TICK = ProjectSettings.get_setting("physics/common/physics_fps")
# gdscript doesn't allow this to be a const if ProjectSettings is used, so it
# has to be manually updated
const PHYSICS_TICK = 30

# The number of steps it takes to travel half a conveyor. I.e. it takes
# 2 * CARGO_STEPS steps to travel an entire conveyor
# Must be divisible by 2 and should probably be a multiple of PHYSICS_TICK
const CARGO_STEPS = 120
# 1 second to move across entire conveyor
const CARGO_STEPS_PER_TICK = 120 / PHYSICS_TICK * 2
