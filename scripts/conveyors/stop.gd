extends "res://scripts/conveyors/base.gd"

const Type = preload("res://scripts/conveyor_type.gd").ConveyorType

func _init(direction) -> void:
    assert(Direction.count_directions(direction) == 1)

    .init(Type.STOP, direction)

func get_look_dir() -> Vector3:
    match self._directions:
        Direction.UP:
            return Vector3(0, 0, -1)
        Direction.DOWN:
            return Vector3(0, 0, 1)
        Direction.LEFT:
            return Vector3(-1, 0, 0)
        Direction.RIGHT:
            return Vector3(1, 0, 0)
        _:
            return Vector3(0, 0, 1)

func get_visualization():
    return preload("res://conveyor_stop.tscn")

func get_type_string():
    match self._directions:
        Direction.UP:
            return "Stop(U)"
        Direction.DOWN:
            return "Stop(D)"
        Direction.LEFT:
            return "Stop(L)"
        Direction.RIGHT:
            return "Stop(R)"
        _:
            return "Stop(?)"

func is_stop() -> bool:
    return true
