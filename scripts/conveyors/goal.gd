extends "res://scripts/conveyors/base.gd"

const Type = preload("res://scripts/conveyor_type.gd").ConveyorType

func _init(direction: int) -> void:
    assert(Direction.count_directions(direction) == 1)

    .init(Type.GOAL, direction)

func get_look_dir() -> Vector3:
    match self._directions:
        Direction.UP:
            return Vector3(0, 0, -1)
        Direction.DOWN:
            return Vector3(0, 0, 1)
        Direction.LEFT:
            return Vector3(-1, 0, 0)
        Direction.RIGHT:
            return Vector3(1, 0, 0)
        _:
            return Vector3(1, 0, 0)

func get_visualization():
    return preload("res://conveyor_goal.tscn")

func get_type_string():
    match self._directions:
        Direction.UP:
            return "Goal(U)"
        Direction.DOWN:
            return "Goal(D)"
        Direction.LEFT:
            return "Goal(L)"
        Direction.RIGHT:
            return "Goal(R)"
        _:
            return "Goal(?)"

func is_static() -> bool:
    return true

func is_goal() -> bool:
    return true
