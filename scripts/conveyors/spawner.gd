extends "res://scripts/conveyors/base.gd"

const Type = preload("res://scripts/conveyor_type.gd").ConveyorType

const TimeConstants = preload("res://scripts/time_constants.gd")

var cooldown: = 0
var running: = false

func _init(direction: int) -> void:
    assert(Direction.count_directions(direction) == 1)

    .init(Type.SPAWNER, direction)

func get_look_dir() -> Vector3:
    match self._directions:
        Direction.UP:
            return Vector3(0, 0, 1)
        Direction.DOWN:
            return Vector3(0, 0, -1)
        Direction.LEFT:
            return Vector3(1, 0, 0)
        Direction.RIGHT:
            return Vector3(-1, 0, 0)
        _:
            return Vector3(0, 0, 1)

func get_visualization():
    return preload("res://conveyor_spawn.tscn")

func get_type_string():
    match self._directions:
        Direction.UP:
            return "Spawner(U)"
        Direction.DOWN:
            return "Spawner(D)"
        Direction.LEFT:
            return "Spawner(L)"
        Direction.RIGHT:
            return "Spawner(R)"
        _:
            return "Spawner(?)"

func is_static() -> bool:
    return true

func is_spawner() -> bool:
    return true

func update():
    .update()

    if cooldown > 0:
        cooldown -= 1

func ready_to_spawn() -> bool:
    return running && cooldown == 0

func spawn():
    cooldown = TimeConstants.PHYSICS_TICK *  2

func start_spawning():
    running = true

func stop_spawning():
    running = false

func get_max_cooldown():
    return TimeConstants.PHYSICS_TICK * 2

func get_current_cooldown():
    return cooldown

func is_running():
    return running
