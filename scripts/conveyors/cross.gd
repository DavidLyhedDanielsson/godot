extends "res://scripts/conveyors/base_junction.gd"

func _init() -> void:
    .init(Type.CROSS, Direction.ALL)

func get_look_dir() -> Vector3:
    return Vector3(-1, 0, 0)

func get_visualization():
    return preload("res://conveyor_cross.tscn")

func get_type_string():
    return "Cross"
