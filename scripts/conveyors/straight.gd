extends "res://scripts/conveyors/base.gd"

const Type = preload("res://scripts/conveyor_type.gd").ConveyorType

enum Directions {
    VERTICAL = Direction.UP | Direction.DOWN,
    HORIZONTAL = Direction.LEFT | Direction.RIGHT
}

func _init(directions: int) -> void:
    assert(Direction.count_directions(directions) == 2)

    .init(Type.STRAIGHT, directions)

func get_look_dir() -> Vector3:
    if Direction.includes(self._directions, Direction.UP):
        return Vector3(0, 0, 1)
    else:
        return Vector3(1, 0, 0)

func get_visualization():
    return preload("res://conveyor.tscn")

func get_type_string():
    if Direction.includes(self._directions, Direction.UP):
        return "straight(V)"
    else:
        return "straight(H)"
