const Direction = preload("res://scripts/direction.gd")

const CARGO_STEPS_PER_TICK = preload('res://scripts/time_constants.gd').CARGO_STEPS_PER_TICK

enum State { RUNNING = 0, STOPPED, STARTING, STOPPING }

enum IncomingOutgoing { INCOMING, OUTGOING }

static func state_to_string(state):
    match state:
        State.RUNNING: return "RUNNING"
        State.STOPPED: return "STOPPED"
        State.STARTING: return "STARTING"
        State.STOPPING: return "STOPPING"
        _: return "Unknown"

var _state: int
var _transition_step: int
var _transition_incoming: int = 0
var _transition_outgoing: int = 0
var _type: int
var _possible_outgoing_directions: int
var _possible_incoming_directions: int
var _directions: int

# First index must be CARGO_STEPS_PER_TICK to keep everything synced
const _speeds = [
    # Max speed
    CARGO_STEPS_PER_TICK,
    # Slowdown/speedup speeds
    7,
    7,
    7,
    6,
    6,
    5,
    5,
    4,
    4,
    3,
    2,
    2,
    1,
    1,
    0,
]

func init(type: int, directions: int) -> void:
    _type = type
    _state = State.STOPPED
    _transition_step = _speeds.size() - 1
    _directions = directions

    # Fake abstract methods
    assert(self.has_method("get_visualization"))
    assert(self.has_method("get_type_string"))
    assert(self.has_method("set_outgoing_direction"))

func get_possible_outgoing_directions() -> int:
    return _possible_outgoing_directions

func get_possible_incoming_directions() -> int:
    return _possible_incoming_directions

func get_type() -> int:
    return _type

func update():
    match _state:
        State.RUNNING: pass
        State.STOPPED: pass
        State.STARTING:
            if _transition_step > 0:
                _transition_step -= 1
            if _transition_step == 0:
                _state = State.RUNNING

            set_incoming_direction(_transition_incoming)
            set_outgoing_direction(_transition_outgoing)
        State.STOPPING:
            if _transition_step < _speeds.size() - 1:
                _transition_step += 1
            if _transition_step == _speeds.size() - 1:
                if (_transition_incoming == Direction.NONE && _transition_outgoing == Direction.NONE)\
                        || (get_possible_incoming_directions() == _transition_incoming && get_possible_outgoing_directions() == _transition_outgoing):
                    _state = State.STOPPED
                else:
                    _state = State.STARTING

func change_direction(incoming, outgoing):
    assert(Direction.includes(_directions, incoming))
    assert(Direction.includes(_directions, outgoing))

    if _transition_incoming == incoming && _transition_outgoing == outgoing:
        return

    if _state == State.STOPPED:
        _state = State.STARTING
    else:
        _state = State.STOPPING

    _transition_incoming = incoming
    _transition_outgoing = outgoing

# TODO: rename all "box" to "cargo"
func move_cargo(_router, box):
    if box.side_of_conveyor == get_possible_incoming_directions():
        box.distance_from_center -= _speeds[_transition_step]
        box.movement_direction = Direction.flip_direction(get_possible_incoming_directions())
        return IncomingOutgoing.INCOMING
    else:
        box.distance_from_center += _speeds[_transition_step]
        box.movement_direction = get_possible_outgoing_directions()
        return IncomingOutgoing.OUTGOING

func start():
    _state = State.STARTING

func force_stop():
    _state = State.STOPPED
    _transition_step = _speeds.size() - 1

func force_running(incoming_direction: int, outgoing_direction: int):
    set_incoming_direction(incoming_direction)
    set_outgoing_direction(outgoing_direction)
    _state = State.RUNNING
    _transition_step = 0

func set_incoming_direction(direction: int) -> void:
    assert(Direction.includes(_directions, direction))
    _possible_incoming_directions = direction

func set_outgoing_direction(direction: int) -> void:
    assert(Direction.includes(_directions, direction))
    _possible_outgoing_directions = direction

func is_stopped():
    return _state == State.STOPPED

func get_allowed_build_directions() -> int:
    return Direction.ALL # TODO: Implement for other types

func is_junction() -> bool:
    return false

func is_static() -> bool:
    return false

func is_stop() -> bool:
    return false

func is_spawner() -> bool:
    return false

func is_goal() -> bool:
    return false

func get_directions() -> int:
    return _directions

func get_look_dir() -> Vector3:
    return Vector3(1, 0, 0)
