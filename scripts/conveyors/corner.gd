extends "res://scripts/conveyors/base.gd"

const NormalVisualization = preload("res://conveyor_corner.tscn")

const Type = preload("res://scripts/conveyor_type.gd").ConveyorType

enum Directions {
    UP_LEFT = Direction.UP | Direction.LEFT,
    UP_RIGHT = Direction.UP | Direction.RIGHT,
    DOWN_LEFT = Direction.DOWN | Direction.LEFT,
    DOWN_RIGHT = Direction.DOWN | Direction.RIGHT,
}

func _init(directions: int) -> void:
    assert(Direction.count_directions(directions) == 2)

    .init(Type.CORNER, directions)

func get_look_dir() -> Vector3:
    match self._directions:
        Directions.UP_LEFT:
            return Vector3(-1, 0, 0)
        Directions.UP_RIGHT:
            return Vector3(0, 0, -1)
        Directions.DOWN_LEFT:
            return Vector3(0, 0, 1)
        Directions.DOWN_RIGHT:
            return Vector3(1, 0, 0)
        _:
            return Vector3(-1, 0, 0)

func get_visualization():
    return NormalVisualization

func get_type_string():
    match self._directions:
        Directions.UP_LEFT:
            return "corner(UL)"
        Directions.UP_RIGHT:
            return "corner(UR)"
        Directions.DOWN_LEFT:
            return "corner(DL)"
        Directions.DOWN_RIGHT:
            return "corner(DR)"
        _:
            return "corner(?)"
