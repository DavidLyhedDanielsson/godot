extends "res://scripts/conveyors/base_junction.gd"

func _init(directions) -> void:
    assert(Direction.count_directions(directions) == 3)

    .init(Type.JUNCTION, directions)

func get_look_dir() -> Vector3:
    match self._directions:
        Directions.HORIZONTAL_UP:
            return Vector3(0, 0, -1)
        Directions.HORIZONTAL_DOWN:
            return Vector3(0, 0, 1)
        Directions.VERTICAL_LEFT:
            return Vector3(-1, 0, 0)
        Directions.VERTICAL_RIGHT:
            return Vector3(1, 0, 0)
        _:
            return Vector3(0, 0, 1)

func get_visualization():
    return preload("res://conveyor_junction.tscn")

func get_type_string():
    match self._directions:
        Directions.HORIZONTAL_UP:
            return "Junction(HU)"
        Directions.HORIZONTAL_DOWN:
            return "Junction(HD)"
        Directions.VERTICAL_LEFT:
            return "Junction(VL)"
        Directions.VERTICAL_RIGHT:
            return "Junction(VR)"
        _:
            return "Junction(?)"

