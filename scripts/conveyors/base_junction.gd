extends "res://scripts/conveyors/base.gd"

const Type = preload("res://scripts/conveyor_type.gd").ConveyorType

const Router = preload("res://scripts/router.gd")
const Cargo = preload("res://scripts/cargo.gd")

enum TrackDirection {
    NONE = 0, NORMAL = 1, INVERTED = 2
}

enum Directions {
    HORIZONTAL_UP = Direction.LEFT | Direction.RIGHT | Direction.UP,
    HORIZONTAL_DOWN = Direction.LEFT | Direction.RIGHT | Direction.DOWN,
    VERTICAL_LEFT = Direction.UP | Direction.DOWN | Direction.LEFT,
    VERTICAL_RIGHT = Direction.UP | Direction.DOWN | Direction.RIGHT,
}

var _track_directions = {
    [Direction.UP, Direction.LEFT]: TrackDirection.NORMAL,
    [Direction.LEFT, Direction.UP]: TrackDirection.INVERTED,
    [Direction.UP, Direction.RIGHT]: TrackDirection.NORMAL,
    [Direction.RIGHT, Direction.UP]: TrackDirection.INVERTED,

    [Direction.DOWN, Direction.LEFT]: TrackDirection.NORMAL,
    [Direction.LEFT, Direction.DOWN]: TrackDirection.INVERTED,
    [Direction.DOWN, Direction.RIGHT]: TrackDirection.NORMAL,
    [Direction.RIGHT, Direction.DOWN]: TrackDirection.INVERTED,

    [Direction.LEFT, Direction.RIGHT]: TrackDirection.NORMAL,
    [Direction.RIGHT, Direction.LEFT]: TrackDirection.INVERTED,
    [Direction.UP, Direction.DOWN]: TrackDirection.NORMAL,
    [Direction.DOWN, Direction.UP]: TrackDirection.INVERTED,
}

class TrackState:
    # TODO: Get rid of this?
    enum State { RUNNING = 0, STOPPED, STARTING, STOPPING }

    var _direction
    var _transition_direction
    var _transition_step
    var _state

    # Can't access State or _speeds from this class, so make them parameters
    # even though they will always be the same
    func _init(state, transition_step):
        _direction = TrackDirection.NONE
        _transition_direction = TrackDirection.NONE
        _transition_step = transition_step
        _state = state

var _track_states = {
    (Direction.UP | Direction.LEFT): TrackState.new(State.STOPPED, _speeds.size() - 1),
    (Direction.UP | Direction.RIGHT): TrackState.new(State.STOPPED, _speeds.size() - 1),
    (Direction.DOWN | Direction.LEFT): TrackState.new(State.STOPPED, _speeds.size() - 1),
    (Direction.DOWN | Direction.RIGHT): TrackState.new(State.STOPPED, _speeds.size() - 1),
    (Direction.UP | Direction.DOWN): TrackState.new(State.STOPPED, _speeds.size() - 1),
    (Direction.LEFT | Direction.RIGHT): TrackState.new(State.STOPPED, _speeds.size() - 1)
}

func init(type, directions) -> void:
    .init(type, directions)
    _possible_outgoing_directions = directions
    _possible_incoming_directions = directions

func update():
    for state in _track_states.values():
        match state._state:
            State.RUNNING: pass
            State.STOPPED: pass
            State.STARTING:
                if state._transition_step > 0:
                    state._transition_step -= 1
                if state._transition_step == 0:
                    state._state = State.RUNNING

                state._direction = state._transition_direction
            State.STOPPING:
                if state._transition_step < _speeds.size() - 1:
                    state._transition_step += 1
                if state._transition_step == _speeds.size() - 1:
                    if (state._transition_direction == TrackDirection.NONE):
                        state._state = State.STOPPED
                    else:
                        state._state = State.STARTING

func change_direction(incoming, outgoing):
    assert(Direction.includes(_directions, incoming))
    assert(Direction.includes(_directions, outgoing))

    var track_state = _track_states[incoming | outgoing]
    var new_direction = _track_directions[[incoming, outgoing]]

    if track_state._transition_direction == new_direction:
        return

    if track_state._state == State.STOPPED:
        track_state._state = State.STARTING
    else:
        track_state._state = State.STOPPING

    track_state._transition_direction = new_direction

const ROUTE_ROTATION = [ Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT ]

func move_cargo(router: Router, box: Cargo):
    var outgoing_direction := get_outgoing_direction(router, box)
    if outgoing_direction == null || outgoing_direction == Direction.NONE:
        if Direction.includes(_directions, box.movement_direction):
            outgoing_direction = box.movement_direction
        else:
            var new_route_index = (ROUTE_ROTATION.find(box.movement_direction) + 1) % ROUTE_ROTATION.size()
            outgoing_direction = ROUTE_ROTATION[new_route_index]

    var incoming_direction
    var speed_mult
    if box.side_of_conveyor == outgoing_direction:
        incoming_direction = box.last_side_of_conveyor
        speed_mult = 1.0
        box.movement_direction = outgoing_direction
    else:
        incoming_direction = box.side_of_conveyor
        speed_mult = -1.0
        box.movement_direction = Direction.flip_direction(incoming_direction)

    var state = _track_states[incoming_direction | outgoing_direction]
    var movement_speed = _speeds[state._transition_step]

    box.distance_from_center += movement_speed * speed_mult

func is_junction() -> bool:
    return true

# This function is duplicated all over the place... Refactor?
func get_outgoing_direction(router: Router, box: Cargo) -> int:
    var next_outgoing_direction := router.get_direction(box.path_id, box.position.x, box.position.y)
    if next_outgoing_direction != Direction.NONE:
        return next_outgoing_direction

    var directions := get_directions()
    if Direction.includes(directions, box.movement_direction):
        return box.movement_direction
    else:
        var route_rotation := [ Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT ]
        # If the cargo is at a junction where it cannot continue in the
        # direction it is currently travelling, that means any of the
        # other three directions work.
        var new_route_index := (route_rotation.find(box.movement_direction) + 1) % route_rotation.size()
        return route_rotation[new_route_index]
