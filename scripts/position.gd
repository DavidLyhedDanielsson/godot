var x: int
var y: int

func _init(x: int, y: int):
    self.x = x
    self.y = y

func is_zero():
    return x == 0 && y == 0

func is_neg_one():
    return x == -1 && y == -1

func move_in_direction(direction: int) -> Object:
    match direction:
        Direction.UP:
            y += 1
        Direction.DOWN:
            y -= 1
        Direction.LEFT:
            x -= 1
        Direction.RIGHT:
            x += 1
    return self

func get_direction_to(other) -> int:
    if other.x < x:
        return Direction.LEFT
    if other.x > x:
        return Direction.RIGHT
    if other.y < y:
        return Direction.DOWN
    if other.y > y:
        return Direction.UP
    return Direction.NONE

func equals(other) -> bool:
    return other.x == x && other.y == y

func is_next_to(other) -> bool:
    return !self.equals(other) && \
        (abs(other.x - x) == 1 && other.y - y == 0) || \
        (abs(other.y - y) == 1 && other.x - x == 0)
