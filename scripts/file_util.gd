const ConveyorType = preload("res://scripts/conveyor_type.gd").ConveyorType

const ConveyorBase = preload("res://scripts/conveyors/base.gd")
const Corner = preload("res://scripts/conveyors/corner.gd")
const Junction = preload("res://scripts/conveyors/junction.gd")
const Straight = preload("res://scripts/conveyors/straight.gd")
const Cross = preload("res://scripts/conveyors/cross.gd")
const Goal = preload("res://scripts/conveyors/goal.gd")
const Spawner = preload("res://scripts/conveyors/spawner.gd")
const Stop = preload("res://scripts/conveyors/stop.gd")

static func _translate_from_direction(direction):
    match direction:
        Direction.UP: return '⇧'
        Direction.DOWN: return '⇩'
        Direction.LEFT: return '⇦'
        Direction.RIGHT: return '⇨'
        Direction.NONE: return 'X'

static func _translate_to_direction(direction):
    match direction:
        '⇧': return Direction.UP
        '⇩': return Direction.DOWN
        '⇦': return Direction.LEFT
        '⇨': return Direction.RIGHT
        'X': return Direction.NONE

static func _translate_from_test_character(character) -> ConveyorBase:
    match character:
        '╝': return Corner.new(Corner.Directions.UP_LEFT)
        '╚': return Corner.new(Corner.Directions.UP_RIGHT)
        '╗': return Corner.new(Corner.Directions.DOWN_LEFT)
        '╔': return Corner.new(Corner.Directions.DOWN_RIGHT)
        '╬': return Cross.new()
        '⤓': return Goal.new(Direction.UP)
        '⤒': return Goal.new(Direction.DOWN)
        '⇥': return Goal.new(Direction.LEFT)
        '⇤': return Goal.new(Direction.RIGHT)
        '╩': return Junction.new(Junction.Directions.HORIZONTAL_UP)
        '╦': return Junction.new(Junction.Directions.HORIZONTAL_DOWN)
        '╣': return Junction.new(Junction.Directions.VERTICAL_LEFT)
        '╠': return Junction.new(Junction.Directions.VERTICAL_RIGHT)
        '↥': return Spawner.new(Direction.UP)
        '↧': return Spawner.new(Direction.DOWN)
        '↤': return Spawner.new(Direction.LEFT)
        '↦': return Spawner.new(Direction.RIGHT)
        '╹': return Stop.new(Direction.UP)
        '╻': return Stop.new(Direction.DOWN)
        '╸': return Stop.new(Direction.LEFT)
        '╺': return Stop.new(Direction.RIGHT)
        '═': return Straight.new(Straight.Directions.HORIZONTAL)
        '║': return Straight.new(Straight.Directions.VERTICAL)
        _: return null

static func _translate_to_test_character(conveyor: ConveyorBase):
    if conveyor == null:
        return ' '

    match conveyor.get_type():
        ConveyorType.CORNER:
            match conveyor.get_directions():
                Corner.Directions.UP_LEFT: return '╝'
                Corner.Directions.UP_RIGHT: return '╚'
                Corner.Directions.DOWN_LEFT: return '╗'
                Corner.Directions.DOWN_RIGHT: return '╔'
        ConveyorType.CROSS:
            return '╬'
        ConveyorType.GOAL:
            match conveyor.get_directions():
                Direction.UP: return '⤓'
                Direction.DOWN: return '⤒'
                Direction.LEFT: return '⇥'
                Direction.RIGHT: return '⇤'
        ConveyorType.JUNCTION:
            match conveyor.get_directions():
                Junction.Directions.HORIZONTAL_UP: return '╩'
                Junction.Directions.HORIZONTAL_DOWN: return '╦'
                Junction.Directions.VERTICAL_LEFT: return '╣'
                Junction.Directions.VERTICAL_RIGHT: return '╠'
        ConveyorType.SPAWNER:
            match conveyor.get_directions():
                Direction.UP: return '↥'
                Direction.DOWN: return '↧'
                Direction.LEFT: return '↤'
                Direction.RIGHT: return '↦'
        ConveyorType.STOP:
            match conveyor.get_directions():
                Direction.UP: return '╹'
                Direction.DOWN: return '╻'
                Direction.LEFT: return '╸'
                Direction.RIGHT: return '╺'
        ConveyorType.STRAIGHT:
            match conveyor.get_directions():
                Straight.Directions.HORIZONTAL: return '═'
                Straight.Directions.VERTICAL: return '║'

static func save_test_scenario(path, world, router):
    var file = File.new()
    file.open(path, File.WRITE)

    for z in range(world.map.size_z() - 1, -1, -1):
        var line = ''
        for x in range(world.map.size_x()):
            line += _translate_to_test_character(world.map.get_at_xz(x, z))
        file.store_line(line)

    file.store_line("")

    for id in router.routes.keys():
        var routes = router.routes.get(id)
        for z in range(world.map.size_z() - 1, -1, -1):
            for x in range(world.map.size_x()):
                var route = routes[z][x]
                if route != Direction.NONE:
                    file.store_line("%s %s %s %s" % [x, z, _translate_from_direction(route), id])

    file.close()

static func load_test_scenario(path, game):
    var file = File.new()
    file.open(path, File.READ)

    var world_lines = []
    var router_lines = []

    var world_loaded = false
    while !file.eof_reached():
        var line = file.get_line()
        if line == '':
            if world_loaded:
                break
            world_loaded = true
        else:
            if world_loaded:
                router_lines.push_front(line)
            else:
                if line != '':
                    world_lines.push_front(line)

    file.close()

    var size_x = world_lines[0].length()
    var size_z = world_lines.size()

    game.create_new_scenario(size_x, size_z)

    for z in range(size_z):
        for x in range(size_x):
            var conveyor = _translate_from_test_character(world_lines[z][x])
            if conveyor != null:
                if conveyor.is_spawner():
                    game.world.add_spawner(game.router, x, z, conveyor.get_directions())
                else:
                    game.world.map.set_at(x, z, conveyor)

    for line in router_lines:
        var info = line.split(" ")
        var x = int(info[0])
        var z = int(info[1])
        var type = info[2]
        var id = int(info[3])
        game.router.create_route(x, z)
        game.router.set_direction(id, x, z, _translate_to_direction(type))

    game.update_conveyors()

static func default_scenario_exists() -> bool:
    return File.new().file_exists("res://default.txt")

static func save_default_scenario(world, router):
    save_test_scenario('res://default.txt', world, router)

static func load_default_scenario(game):
    load_test_scenario("res://default.txt", game)
