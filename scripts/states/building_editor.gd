const Position = preload("res://scripts/position.gd")

enum BUILD_TYPE { STOP, SPAWN, GOAL }

# Some more comments are available in states/building.gd

var build_from: = Position.new(-1, -1)
var build_type: int = -1
var mouse_down = false
