const Position = preload("res://scripts/position.gd")

# Position that the player is currently building from - the "parent" tile
# if empty, no building is currently taking place
var build_from: = Position.new(0, 0)

var first_conveyor: Object

# {position: Position, type: ConveyorType}
var previews: Array = []

func get_preview_at_position(position):
    #for preview in previews:
    for i in range(previews.size() - 1, -1, -1):
        var preview = previews[i]
        if preview.position.equals(position):
            return preview
    return null
