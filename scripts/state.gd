const NoneState = preload("states/none.gd")
const BuildingState = preload("states/building.gd")
const BuildingEditorState = preload("states/building_editor.gd")
const RoutingState = preload("states/routing.gd")

var none_state = NoneState.new()
var building_state
var building_editor_state
var routing_state

# Substitution so that we don't have to write "State.State.NONE" when including
# this class in other files
const NONE = 0
const BUILDING = 1
const BUILDING_EDITOR = 2
const ROUTING = 3

var current_state = NONE

func state_to_string(state: int) -> String:
    match state:
        NONE:
            return "NONE"
        BUILDING:
            return "BUILDING"
        BUILDING_EDITOR:
            return "BUILDING_EDITOR"
        ROUTING:
            return "ROUTING"
        _:
            return "UNKNOWN"

func get_state() -> int:
    return current_state

func current_state_is(state) -> bool:
    return current_state == state

func is_none() -> bool:
    return current_state == NONE

func is_building() -> bool:
    return current_state == BUILDING

func is_building_editor() -> bool:
    return current_state == BUILDING_EDITOR

func is_routing() -> bool:
    return current_state == ROUTING

func set_none() -> void:
    current_state = NONE
    none_state = NoneState.new()
    building_state = null
    building_editor_state = null
    routing_state = null

func set_building() -> void:
    current_state = BUILDING
    none_state = null
    building_state = BuildingState.new()
    building_editor_state = null
    routing_state = null

func set_building_editor() -> void:
    current_state = BUILDING_EDITOR
    none_state = null
    building_state = null
    building_editor_state = BuildingEditorState.new()
    routing_state = null

func set_routing() -> void:
    current_state = ROUTING
    none_state = null
    building_state = null
    building_editor_state = null
    routing_state = RoutingState.new()

func get_state_info(state: int) -> Object:
    if current_state != state:
        DebugInformation.print('Getting info for state ',
                               state_to_string(state),
                               ', but current state is ',
                               state_to_string(current_state))

    match state:
        NONE:
            return none_state
        BUILDING:
            return building_state
        BUILDING_EDITOR:
            return building_editor_state
        ROUTING:
            return routing_state
        _:
            return null
