extends Camera

var direction: Vector3 = Vector3(0, 0, 0)

var up = false
var down = false
var left = false
var right = false
var zoom_out = false
var zoom_in = false

var speed = 5.0

func input(event: InputEvent) -> void:
    var new_direction: Vector3 = Vector3(0, 0, 0)
    up = (up || event.is_action_pressed("up")) && !event.is_action_released("up")
    down = (down || event.is_action_pressed("down")) && !event.is_action_released("down")
    left = (left || event.is_action_pressed("left"))&& !event.is_action_released("left")
    right = (right || event.is_action_pressed("right")) && !event.is_action_released("right")

    zoom_out = (zoom_out || event.is_action_pressed("ui_page_up")) && !event.is_action_released("ui_page_up")
    zoom_in = (zoom_in || event.is_action_pressed("ui_page_down")) && !event.is_action_released("ui_page_down")

    if up:
        new_direction.z -= 1.0
    if down:
        new_direction.z += 1.0
    if left:
        new_direction.x -= 1.0
    if right:
        new_direction.x += 1.0
    if zoom_out:
        new_direction.y += 1.0
    if zoom_in:
        new_direction.y -= 1.0
    self.direction = new_direction * speed

func _process(delta: float) -> void:
    if !Input.is_action_pressed("up") && !Input.is_action_pressed("down"):
        self.direction.z = 0.0
    if !Input.is_action_pressed("left") && !Input.is_action_pressed("right"):
        self.direction.x = 0.0

    self.transform.origin += direction * delta
