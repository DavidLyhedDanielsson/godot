extends Spatial

const Position = preload("res://scripts/position.gd")

# Position of the conveyor this cargo is currently being moved by
var position: Position
# How far away from the center of the conveyor at position
# this cargo is. Have a look at CARGO_STEPS in time_constants.gd.
var distance_from_center: int
# Used in combination with distance_from_center to determine graphical position
var side_of_conveyor
# Which direction the cargo is moving
var movement_direction: int
# Used to determine which "track" of a junction the cargo is at, since only the
# curren side of conveyor is not enough; if the cargo is moving UP and out of a
# junction, did it come from the left or right?
var last_side_of_conveyor
# Path id = spawner id, used to route the cargo through junctions according to
# the spawner's route config
var path_id: int

# Where the cargo should be drawn according to the physics engine, i.e. the
# value is updated in _physics_process and _not_ in _process
var draw_position: Vector3
# Whether or not last_draw_position is set
var last_set = false
# Where the cargo was last drawn according to the physics engine. Used to
# interpolate between last_draw_position and draw_position to smootly move the
# cargo when the game is being ran at higher frame rates than the physics tick.
# For example, if the cargo position is updated in _process_physics (called 30
# times per seconds), but the game should be rendered 60, 120, 144, or any
# other framerate, the cargo will stutter forward if it is not interpolated
var last_draw_position: Vector3

func _init(path_id: int, conveyor_x: int, conveyor_z: int, side_of_conveyor: int):
    self.position = Position.new(conveyor_x, conveyor_z)
    self.distance_from_center = 0
    self.side_of_conveyor = side_of_conveyor
    self.last_side_of_conveyor = side_of_conveyor
    self.path_id = path_id
    self.last_set = false
    self.movement_direction = Direction.NONE
