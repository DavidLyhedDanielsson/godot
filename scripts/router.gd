const Position = preload("res://scripts/position.gd")

const Cargo = preload("res://scripts/cargo.gd")

var routes: Dictionary
var cargo_in_route: Dictionary

class RouteInfo:
    var cargo_count: int
    var cargoes: Array

    func _init():
        cargo_count = 0;
        cargoes = []

var world_size_x: int
var world_size_z: int

func _init(world_size_x: int, world_size_z: int) -> void:
    self.world_size_x = world_size_x
    self.world_size_z = world_size_z

func get_small_big_id(start_x: int, start_y: int, end_x: int, end_y: int) -> Array:
    var start_id = create_id(start_x, start_y)
    var end_id = create_id(end_x, end_y)

    var small_id = start_id if start_id < end_id else end_id
    var big_id = end_id if start_id < end_id else start_id

    return [small_id, big_id]

func route_count(start_x: int, start_y: int, end_x: int, end_y: int) -> int:
    var ids = get_small_big_id(start_x, start_y, end_x, end_y)
    var small_id = ids[0]
    var big_id = ids[1]
    return cargo_in_route.get(small_id, {}).get(big_id, RouteInfo.new()).cargo_count

func enter_route(box: Cargo, start_x: int, start_y: int, end_x: int, end_y: int) -> void:
    var ids = get_small_big_id(start_x, start_y, end_x, end_y)
    var small_id = ids[0]
    var big_id = ids[1]

    var outer
    if cargo_in_route.has(small_id):
        outer = cargo_in_route.get(small_id)
    else:
        outer = {}
        cargo_in_route[small_id] = outer

    var route_info = outer.get(big_id, RouteInfo.new())
    route_info.cargo_count += 1
    route_info.cargoes.append(box)
    outer[big_id] = route_info

func exit_route(box: Cargo, start_x: int, start_y: int, end_x: int, end_y: int) -> void:
    var ids = get_small_big_id(start_x, start_y, end_x, end_y)
    var small_id = ids[0]
    var big_id = ids[1]

    var route_info: RouteInfo = cargo_in_route.get(small_id, {}).get(big_id)
    if route_info.cargo_count == 1:
        if (cargo_in_route[small_id] as Dictionary).size() == 0:
            cargo_in_route.erase(small_id)
        else:
            (cargo_in_route[small_id] as Dictionary).erase(big_id)
    else:
        route_info.cargo_count -= 1
        route_info.cargoes.erase(box)
        cargo_in_route[small_id][big_id] = route_info

func get_cargo_in_route(start_x: int, start_y: int, end_x: int, end_y: int) -> Array:
    var ids = get_small_big_id(start_x, start_y, end_x, end_y)
    var small_id = ids[0]
    var big_id = ids[1]
    return cargo_in_route.get(small_id, {}).get(big_id, RouteInfo.new()).cargoes

func create_id(id_x: int, id_z: int) -> int:
    return (id_x & 0xFFFFFFFF) | ((id_z & 0xFFFFFFFF) << 32)

func decode_id(id) -> Position:
    return Position.new(id & 0xFFFFFFFF, ((id >> 32) & 0xFFFFFFFF))

func create_route(id_x: int, id_z: int):
    var directions = []
    directions.resize(world_size_z)
    for z in range(world_size_z):
        var row = []
        row.resize(world_size_x)
        for x in range(world_size_x):
            row[x] = Direction.NONE
        directions[z] = row
    var id = create_id(id_x, id_z)
    routes[id] = directions

# Sets the direction of a junction
#
# In the future id_x and id_y will be used to identify which spawner the
# direction belongs to, but currently only one spawner is supported.
#
# @param {int} id_x
# @param {int} id_z
# @param {int} x
# @param {int} z
# @param {Direction} direction
func set_direction(id: int, x: int, z: int, direction: int) -> void:
    if x < 0 || x >= self.world_size_x:
        return
    if z < 0 || z >= self.world_size_z:
        return

    if !routes.has(id):
        return
    routes.get(id)[z][x] = direction

func delete_route(id_x: int, id_z: int):
    var id = create_id(id_x, id_z)
    routes.erase(id)

# Gets the direction of a junction
#
# In the future id_x and id_y will be used to identify which spawner the
# direction belongs to, but currently only one spawner is supported.
#
# @param {int} id_x
# @param {int} id_z
# @param {int} x
# @param {int} z
func get_direction(id: int, x: int, z: int) -> int:
    if x < 0 || x >= self.world_size_x:
        return Direction.NONE
    if z < 0 || z >= self.world_size_z:
        return Direction.NONE

    if !routes.has(id):
        return Direction.NONE

    return routes.get(id)[z][x]
