const Position = preload("res://scripts/position.gd")
const Base = preload("res://scripts/conveyors/base.gd")
const Array2D = preload("res://scripts/array_2d.gd")

var _conveyors: Array2D = null

func _init(world_size_x: int, world_size_z: int) -> void:
    _conveyors = Array2D.new(world_size_x, world_size_z)

# Checks whether or not the given position is within world grid bounds.
#
# @param {int} x - x-position
# @param {int} z - z-position
# @returns {boolean}
func is_valid(x: int, z: int) -> bool:
    return x >= 0 && x < size_x() && z >= 0 && z < size_z()

# Sets the tile at the given position to the given type.
#
# @param {int} x - x-position
# @param {int} z - z-position
# @param {ConveyorType} type - conveyor type to place
func set_at(x: int, z: int, type: Object) -> void:
    _conveyors.s(x, z, type)

# Gets the tile at the given position.
#
# @param {Position} position
# @returns {ConveyorType}
func get_at(position: Position) -> Base:
    return _conveyors.g(position.x, position.y)

func get_at_xz(x: int, z: int) -> Base:
    return _conveyors.g(x, z)

# Gets the tile at the given position if the position is within bounds,
# otherwise ConveyorType.NONE.
#
# @param {Position} position
# @returns {ConveyorType}
func try_get_at(position: Position) -> Base:
    return _conveyors.try_g(position.x, position.y)

func try_get_at_xz(x: int, z: int) -> Base:
    return _conveyors.try_g(x, z)

func size_x() -> int:
    return _conveyors.size_x()

func size_z() -> int:
    return _conveyors.size_y()

func get_size() -> Position:
    return Position.new(size_x(), size_z())

# Given a ray, returns where this ray intersects the world grid.
#
# @param {Vector3} ray_origin - position of the ray's origin
# @param {Vector3} ray_direction - the direction of the ray
# @returns {[int, int]} - [x, z] hit position
func pick(ray_origin: Vector3, ray_direction: Vector3) -> Position:
    var dist := -ray_origin.y / ray_direction.y
    var hit := (ray_origin + ray_direction * dist) + Vector3(0.5, 0, 0.5)
    return Position.new(floor(hit.x) as int, -floor(hit.z) as int)
