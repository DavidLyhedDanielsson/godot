extends Spatial

const FileUtil = preload("res://scripts/file_util.gd")
const Position = preload("res://scripts/position.gd")
const TimeConstants = preload("time_constants.gd")

const World = preload("world.gd")
const Router = preload("router.gd")
const Input = preload("input.gd")

const ConveyorType = preload("conveyor_type.gd").ConveyorType
const Base = preload("res://scripts/conveyors/base.gd")
const Corner = preload("res://scripts/conveyors/corner.gd")
const Junction = preload("res://scripts/conveyors/base_junction.gd")
const Goal = preload("res://scripts/conveyors/goal.gd")
const Stop = preload("res://scripts/conveyors/stop.gd")
const Spawner = preload("res://scripts/conveyors/spawner.gd")

const Cargo = preload("cargo.gd")
const State = preload("state.gd")
const StateNone = preload("states/none.gd")
const StateBuilding = preload("states/building.gd")
const StateBuildingEditor = preload("states/building_editor.gd")
const StateRouting = preload("states/routing.gd")

const ArrowUnselected = preload("res://arrow_unselected.tscn")
const ArrowSelected = preload("res://arrow_selected.tscn")
const ConveyorDebug = preload("res://conveyor_debug.tscn")
const CargoDebug = preload("res://cargo_debug.tscn")

const BuildingState = preload("states/building.gd")
const RoutingState = preload("states/routing.gd")
const BuildingEditorState = preload("states/building_editor.gd")

const WORLD_SIZE_X := 30
const WORLD_SIZE_Z := 30

class Conveyor:
    const Base = preload("res://scripts/conveyors/base.gd")

    var _script: Base
    var _node: Node

    func _init(script: Base, node: Node):
        _script = script
        _node = node

const conveyors: Array = []
const preview_arrows: Array = []

var world := World.new(0, 0)
var router := Router.new(0, 0)
var state := preload("state.gd").new()

enum SimulationState {
    RUN, STOP, STEP
}
var _simulation_state: int = SimulationState.RUN

func create_new_scenario(size_x: int, size_z: int) -> void:
    var middle_x := size_x / 2
    var middle_z := size_z / 2

    world = World.new(size_x, size_z)
    router = Router.new(size_x, size_z)
    $Ground.create(size_x, size_z)

    $Camera.look_at_from_position(Vector3(middle_x, 5.0, -middle_z + 3),
                                 Vector3(middle_x, 0.0, -middle_z),
                                 Vector3(0, 1, 0))

    update_conveyors()
    clear_route_preview()
    remove_spawner_controls()
    state.set_none()

func _ready() -> void:
    update_time_controls(_simulation_state)

    if FileUtil.default_scenario_exists():
        FileUtil.load_default_scenario(self)
    else:
        create_new_scenario(WORLD_SIZE_X, WORLD_SIZE_Z)

    var menu_bar: PanelContainer = find_node('MenuBar')
    var conveyor_picker: HBoxContainer = find_node('ConveyorPicker')

    conveyor_picker.add_button("res://conveyor_spawn.tscn", StateBuildingEditor.BUILD_TYPE.SPAWN, 'Spawn')
    conveyor_picker.add_button("res://conveyor_goal.tscn", StateBuildingEditor.BUILD_TYPE.GOAL, 'Goal')
    conveyor_picker.connect("button_toggled", self, "select_conveyor")

    var conveyor_picker_background: PanelContainer = find_node('ConveyorPickerBackground')
    conveyor_picker_background.visible = (menu_bar.find_node("Editor") as CheckButton).pressed

    $SpawnControls.hide()
    $SpawnControls/Start.connect("pressed", self, "_on_StartSpawner_pressed")
    $SpawnControls/Stop.connect("pressed", self, "_on_StopSpawner_pressed")

    (menu_bar.find_node("File", true) as MenuButton).get_popup().connect("id_pressed", self, "_on_File_pressed")
    (menu_bar.find_node("Debug", true) as MenuButton).get_popup().connect("id_pressed", self, "_on_Debug_pressed")
    (menu_bar.find_node("Editor", true) as CheckButton).connect("toggled", self, "_on_Editor_toggled")

func select_conveyor(name: int, value: bool) -> void:
    if value:
        state.set_building_editor()
        var state_info := state.get_state_info(State.BUILDING_EDITOR) as BuildingEditorState
        state_info.build_type = name
        update_conveyors();
        clear_route_preview()
        remove_spawner_controls()
    else:
        state.set_none()
        update_conveyors();

func create_spawner_controls() -> void:
    $SpawnControls.show()
    update_spawner_controls()

func remove_spawner_controls() -> void:
    $SpawnControls.hide()

func _on_StartSpawner_pressed() -> void:
    var route_from: Position = (state.get_state_info(State.ROUTING) as RoutingState).route_from
    var conveyor := world.map.get_at(route_from)
    conveyor.start_spawning()

func _on_StopSpawner_pressed() -> void:
    var route_from: Position = (state.get_state_info(State.ROUTING) as RoutingState).route_from
    var conveyor := world.map.get_at(route_from)
    conveyor.stop_spawning()

func update_spawner_controls() -> void:
    var route_from: Position = (state.get_state_info(State.ROUTING) as RoutingState).route_from
    var center_position := ($Camera as Camera).unproject_position(Vector3(route_from.x, 0, -route_from.y))
    $SpawnControls.set_global_position(center_position)
    $SpawnControls/Start.set_position(Vector2(32.0, -32))
    $SpawnControls/Stop.set_position(Vector2(-32.0, -32))

var spawner_timers := {}

func update_spawner_timer(conveyor: Base, x: int, z: int):
    var spawner_timer: TextureProgress = spawner_timers.get([x, z], null)
    if spawner_timer == null:
        spawner_timer = TextureProgress.new()
        var texture := preload("res://gui/game/circular_progress.png")
        spawner_timer.fill_mode = TextureProgress.FILL_COUNTER_CLOCKWISE
        spawner_timer.texture_progress = texture
        spawner_timer.set_min(0)
        spawner_timer.set_max(conveyor.get_max_cooldown())
        add_child(spawner_timer)
        spawner_timers[[x, z]] = spawner_timer

    spawner_timer.set_max(conveyor.get_max_cooldown())
    spawner_timer.set_value(conveyor.get_current_cooldown())

    var progress := conveyor.get_current_cooldown() as float / conveyor.get_max_cooldown() as float

    var red := Color("#e30000")
    var green := Color("#63d664")

    # Get off the reds faster, linger on greens longer
    var progress_squared := progress * progress
    # Lerping colors is wrong, but it's quick, easy, and good enough for now
    spawner_timer.tint_progress = red * progress_squared + green * (1.0 - progress_squared)

    var size := spawner_timer.rect_size

    var screen_position := ($Camera as Camera).unproject_position(Vector3(x, 1.25, -(z + 0.25))) - Vector2(size.x / 2.0, size.y / 2.0)
    spawner_timer.set_global_position(screen_position)

func _unhandled_input(event) -> void:
    var camera := $Camera as Camera

    match state.get_state():
        State.NONE:
            Input.input_none(event, camera, self)

            if state.is_routing():
                create_spawner_controls()
        State.BUILDING:
            Input.input_building(event, camera, self)
        State.BUILDING_EDITOR:
            Input.input_building_editor(event, camera, self)
        State.ROUTING:
            Input.input_routing(event, camera, self)

            if !state.is_routing():
                remove_spawner_controls()
            else:
                update_spawner_controls()

    if event is InputEventKey:
        if event.pressed:
            if event.scancode == KEY_Q:
                var world_size := world.map.get_size()
                for z in world_size.y:
                    for x in world_size.x:
                        world.force_spawn(router, x, z)
            elif event.scancode == KEY_F1:
                DebugInformation.print("Updating conveyors")
                update_conveyors()

        camera.input(event)

var time_acc := 0.0
var max_time_acc := 0.0

func _process(delta) -> void:
    time_acc += delta
    if time_acc < 0:
        time_acc = 0
    max_time_acc = max(max_time_acc, time_acc)

    var box_multi_mesh: MultiMesh = $BoxMultiMesh.multimesh
    box_multi_mesh.instance_count = world.cargoes.size()

    for i in range(world.cargoes.size()):
        var box: Cargo = world.cargoes[i]

        var alpha := time_acc / (1.0 / TimeConstants.PHYSICS_TICK)

        var draw_position := box.draw_position * alpha + box.last_draw_position * (1.0 - alpha)

        var position := Transform().translated(draw_position)
        box_multi_mesh.set_instance_transform(i, position)
        box.last_draw_position = draw_position

    for c in conveyors:
        var conveyor := c as Conveyor
        var gear = conveyor._node.find_node("Gear") as GeometryInstance
        if gear:
            # 2PI * delta gives 1 rotation per second
            # Multiplying by the conveyor speed just to visualize the speed
            # Dividing by steps per tick
            var rotational_delta = PI * 2.0 * delta * Base._speeds[conveyor._script._transition_step] / 15.0
            if conveyor._script._type == ConveyorType.STRAIGHT:
                var out_dir = conveyor._script.get_possible_outgoing_directions()
                match out_dir:
                    Direction.UP:
                        gear.rotate_x(rotational_delta)
                    Direction.DOWN:
                        gear.rotate_x(-rotational_delta)
                    Direction.LEFT:
                        gear.rotate_x(rotational_delta)
                    Direction.RIGHT:
                        gear.rotate_x(-rotational_delta)
            elif conveyor._script._type == ConveyorType.CORNER:
                var out_dir = conveyor._script.get_directions()
                match out_dir:
                    Corner.Directions.UP_LEFT:
                        if conveyor._script.get_possible_outgoing_directions() == Direction.UP:
                            gear.rotate_object_local(Vector3(0, 0, 1), rotational_delta)
                        else:
                            gear.rotate_object_local(Vector3(0, 0, 1), -rotational_delta)
                    Corner.Directions.UP_RIGHT:
                        if conveyor._script.get_possible_outgoing_directions() == Direction.UP:
                            gear.rotate_object_local(Vector3(0, 0, 1), -rotational_delta)
                        else:
                            gear.rotate_object_local(Vector3(0, 0, 1), rotational_delta)
                    Corner.Directions.DOWN_LEFT:
                        if conveyor._script.get_possible_outgoing_directions() == Direction.DOWN:
                            gear.rotate_object_local(Vector3(0, 0, 1), -rotational_delta)
                        else:
                            gear.rotate_object_local(Vector3(0, 0, 1), rotational_delta)
                    Corner.Directions.DOWN_RIGHT:
                        if conveyor._script.get_possible_outgoing_directions() == Direction.DOWN:
                            gear.rotate_object_local(Vector3(0, 0, 1), rotational_delta)
                        else:
                            gear.rotate_object_local(Vector3(0, 0, 1), -rotational_delta)
        else:
            var gear0 = conveyor._node.find_node("Gear0") as GeometryInstance
            var gear1 = conveyor._node.find_node("Gear1") as GeometryInstance
            var gear2 = conveyor._node.find_node("Gear2") as GeometryInstance
            if conveyor._script._type == ConveyorType.JUNCTION:
                var out_dir = conveyor._script.get_directions()
                match out_dir:
                    Junction.Directions.HORIZONTAL_UP:
                        var state0 = conveyor._script._track_states[Direction.LEFT | Direction.RIGHT]
                        var rotational_delta0 = PI * 2.0 * delta * Base._speeds[state0._transition_step] / 15.0
                        if state0._direction == Junction.TrackDirection.NORMAL:
                            gear0.rotate_object_local(Vector3(0, 1, 0), rotational_delta0)
                        else:
                            gear0.rotate_object_local(Vector3(0, 1, 0), -rotational_delta0)

                        var state1 = conveyor._script._track_states[Direction.RIGHT | Direction.UP]
                        var rotational_delta1 = PI * 2.0 * delta * Base._speeds[state1._transition_step] / 15.0
                        if state1._direction == Junction.TrackDirection.NORMAL:
                            gear1.rotate_object_local(Vector3(0, 0, 1), rotational_delta1)
                        else:
                            gear1.rotate_object_local(Vector3(0, 0, 1), -rotational_delta1)

                        var state2 = conveyor._script._track_states[Direction.LEFT | Direction.UP]
                        var rotational_delta2 = PI * 2.0 * delta * Base._speeds[state2._transition_step] / 15.0
                        if state2._direction == Junction.TrackDirection.NORMAL:
                            gear2.rotate_object_local(Vector3(0, 0, 1), rotational_delta2)
                        else:
                            gear2.rotate_object_local(Vector3(0, 0, 1), -rotational_delta2)
                    Junction.Directions.HORIZONTAL_DOWN:
                        var state0 = conveyor._script._track_states[Direction.LEFT | Direction.RIGHT]
                        var rotational_delta0 = PI * 2.0 * delta * Base._speeds[state0._transition_step] / 15.0
                        if state0._direction == Junction.TrackDirection.NORMAL:
                            gear0.rotate_object_local(Vector3(0, 1, 0), -rotational_delta0)
                        else:
                            gear0.rotate_object_local(Vector3(0, 1, 0), rotational_delta0)

                        var state1 = conveyor._script._track_states[Direction.RIGHT | Direction.DOWN]
                        var rotational_delta1 = PI * 2.0 * delta * Base._speeds[state1._transition_step] / 15.0
                        if state1._direction == Junction.TrackDirection.NORMAL:
                            gear1.rotate_object_local(Vector3(0, 0, 1), rotational_delta1)
                        else:
                            gear1.rotate_object_local(Vector3(0, 0, 1), -rotational_delta1)

                        var state2 = conveyor._script._track_states[Direction.LEFT | Direction.DOWN]
                        var rotational_delta2 = PI * 2.0 * delta * Base._speeds[state2._transition_step] / 15.0
                        if state2._direction == Junction.TrackDirection.NORMAL:
                            gear2.rotate_object_local(Vector3(0, 0, 1), rotational_delta2)
                        else:
                            gear2.rotate_object_local(Vector3(0, 0, 1), -rotational_delta2)
                    Junction.Directions.VERTICAL_LEFT:
                        var state0 = conveyor._script._track_states[Direction.UP | Direction.DOWN]
                        var rotational_delta0 = PI * 2.0 * delta * Base._speeds[state0._transition_step] / 15.0
                        if state0._direction == Junction.TrackDirection.NORMAL:
                            gear0.rotate_object_local(Vector3(0, 1, 0), -rotational_delta0)
                        else:
                            gear0.rotate_object_local(Vector3(0, 1, 0), rotational_delta0)

                        var state1 = conveyor._script._track_states[Direction.UP | Direction.LEFT]
                        var rotational_delta1 = PI * 2.0 * delta * Base._speeds[state1._transition_step] / 15.0
                        if state1._direction == Junction.TrackDirection.NORMAL:
                            gear1.rotate_object_local(Vector3(0, 0, 1), -rotational_delta1)
                        else:
                            gear1.rotate_object_local(Vector3(0, 0, 1), rotational_delta1)

                        var state2 = conveyor._script._track_states[Direction.DOWN | Direction.LEFT]
                        var rotational_delta2 = PI * 2.0 * delta * Base._speeds[state2._transition_step] / 15.0
                        if state2._direction == Junction.TrackDirection.NORMAL:
                            gear2.rotate_object_local(Vector3(0, 0, 1), -rotational_delta2)
                        else:
                            gear2.rotate_object_local(Vector3(0, 0, 1), rotational_delta2)
                    Junction.Directions.VERTICAL_RIGHT:
                        var state0 = conveyor._script._track_states[Direction.UP | Direction.DOWN]
                        var rotational_delta0 = PI * 2.0 * delta * Base._speeds[state0._transition_step] / 15.0
                        if state0._direction == Junction.TrackDirection.NORMAL:
                            gear0.rotate_object_local(Vector3(0, 1, 0), rotational_delta0)
                        else:
                            gear0.rotate_object_local(Vector3(0, 1, 0), -rotational_delta0)

                        var state1 = conveyor._script._track_states[Direction.DOWN | Direction.RIGHT]
                        var rotational_delta1 = PI * 2.0 * delta * Base._speeds[state1._transition_step] / 15.0
                        if state1._direction == Junction.TrackDirection.NORMAL:
                            gear1.rotate_object_local(Vector3(0, 0, 1), -rotational_delta1)
                        else:
                            gear1.rotate_object_local(Vector3(0, 0, 1), rotational_delta1)

                        var state2 = conveyor._script._track_states[Direction.UP | Direction.RIGHT]
                        var rotational_delta2 = PI * 2.0 * delta * Base._speeds[state2._transition_step] / 15.0
                        if state2._direction == Junction.TrackDirection.NORMAL:
                            gear2.rotate_object_local(Vector3(0, 0, 1), -rotational_delta2)
                        else:
                            gear2.rotate_object_local(Vector3(0, 0, 1), rotational_delta2)
            elif conveyor._script._type == ConveyorType.CROSS:
                var gear3 = conveyor._node.find_node("Gear3") as GeometryInstance
                var gear4 = conveyor._node.find_node("Gear4") as GeometryInstance
                var gear5 = conveyor._node.find_node("Gear5") as GeometryInstance

                var state0 = conveyor._script._track_states[Direction.UP | Direction.DOWN]
                var rotational_delta0 = PI * 2.0 * delta * Base._speeds[state0._transition_step] / 15.0
                if state0._direction == Junction.TrackDirection.NORMAL:
                    gear0.rotate_object_local(Vector3(0, 0, 1), -rotational_delta0)
                else:
                    gear0.rotate_object_local(Vector3(0, 0, 1), rotational_delta0)

                var state1 = conveyor._script._track_states[Direction.LEFT | Direction.RIGHT]
                var rotational_delta1 = PI * 2.0 * delta * Base._speeds[state1._transition_step] / 15.0
                if state1._direction == Junction.TrackDirection.NORMAL:
                    gear1.rotate_object_local(Vector3(0, 0, 1), rotational_delta1)
                else:
                    gear1.rotate_object_local(Vector3(0, 0, 1), -rotational_delta1)

                var state2 = conveyor._script._track_states[Direction.DOWN | Direction.LEFT]
                var rotational_delta2 = PI * 2.0 * delta * Base._speeds[state2._transition_step] / 15.0
                if state2._direction == Junction.TrackDirection.NORMAL:
                    gear2.rotate_object_local(Vector3(0, 0, 1), -rotational_delta2)
                else:
                    gear2.rotate_object_local(Vector3(0, 0, 1), rotational_delta2)

                var state3 = conveyor._script._track_states[Direction.UP | Direction.LEFT]
                var rotational_delta3 = PI * 2.0 * delta * Base._speeds[state3._transition_step] / 15.0
                if state3._direction == Junction.TrackDirection.NORMAL:
                    gear3.rotate_object_local(Vector3(0, 0, 1), -rotational_delta3)
                else:
                    gear3.rotate_object_local(Vector3(0, 0, 1), rotational_delta3)

                var state4 = conveyor._script._track_states[Direction.UP | Direction.RIGHT]
                var rotational_delta4 = PI * 2.0 * delta * Base._speeds[state4._transition_step] / 15.0
                if state4._direction == Junction.TrackDirection.NORMAL:
                    gear4.rotate_object_local(Vector3(0, 0, 1), rotational_delta4)
                else:
                    gear4.rotate_object_local(Vector3(0, 0, 1), -rotational_delta4)

                var state5 = conveyor._script._track_states[Direction.DOWN | Direction.RIGHT]
                var rotational_delta5 = PI * 2.0 * delta * Base._speeds[state5._transition_step] / 15.0
                if state5._direction == Junction.TrackDirection.NORMAL:
                    gear5.rotate_object_local(Vector3(0, 0, 1), rotational_delta5)
                else:
                    gear5.rotate_object_local(Vector3(0, 0, 1), -rotational_delta5)

# delta is assumed to be kept more or less constant by the engine
func _physics_process(_delta) -> void:
    time_acc -= (1.0 / TimeConstants.PHYSICS_TICK)

    if _simulation_state == SimulationState.STOP:
        debug_conveyor()
        debug_cargo()
        return
    elif _simulation_state == SimulationState.STEP:
        _simulation_state = SimulationState.STOP

    if state.is_routing():
        update_spawner_controls()

    if world.update_conveyors(router):
        update_conveyors()

    for i in range(world.cargoes.size()):
        var box: Cargo = world.cargoes[i]

        var distance := box.distance_from_center / float(TimeConstants.CARGO_STEPS) * 0.5 # 0.5 = half a tile

        var offset: Vector3
        match box.side_of_conveyor:
            Direction.UP:
                offset = Vector3(0, 0, -distance)
            Direction.DOWN:
                offset = Vector3(0, 0, distance)
            Direction.LEFT:
                offset = Vector3(-distance, 0, 0)
            Direction.RIGHT:
                offset = Vector3(distance, 0, 0)
            _: offset = Vector3(0, 0, 0)

        var new_position: = Vector3(box.position.x, 0.25, -box.position.y) + offset
        if box.last_set:
            box.last_draw_position = box.draw_position
        else:
            box.last_draw_position = new_position
            box.last_set = true
        box.draw_position = new_position

    for z in range(world.map.size_z()):
        for x in range(world.map.size_x()):
            var conveyor := world.map.get_at_xz(x, z)
            if conveyor != null && conveyor.is_spawner():
                #if conveyor.get_current_cooldown() != 0 || conveyor.is_running():
                update_spawner_timer(conveyor, x, z)

    debug_conveyor()
    debug_cargo()
    debug_routes()
    debug_tiles()

func create_preview_material() -> Array:
    var materials := []
    for i in range(0, 4):
        var material := SpatialMaterial.new()
        #material.flags_unshaded = true
        material.flags_transparent = true
        material.albedo_color = Color(1.0, 1.0, 1.0, 0.1 + 0.1 * i)
        materials.append(material)
    return materials

export(Array, SpatialMaterial) var preview_materials := create_preview_material()

# Adds the given conveyor type at the given position
#
# @param {ConveyorType} type
# @param {int} x - x-position of the new conveyor
# @param {int} z - z-position of the new conveyor
# @param {boolean} preview - whether or not to use the preview material
func add_conveyor(type: Base, x: int, z: int, preview_depth: int = -1) -> void:
    var conveyor: Node = type.get_visualization().instance()

    if preview_depth != -1:
        var child = conveyor.get_child(0)
        child.set_material_override(preview_materials[preview_depth])

    if conveyor:
        self.add_child(conveyor)
        conveyors.push_front(Conveyor.new(type, conveyor))
        var look_dir := type.get_look_dir()
        conveyor.look_at(look_dir, Vector3(0.0, 1.0, 0.0))
        conveyor.transform.origin = Vector3(x, 0, -z)

func update_conveyors() -> void:
    for child in conveyors:
        child._node.queue_free()
    conveyors.clear()

    match state.get_state():
        state.NONE, state.ROUTING:
            for x in range(world.map.size_x()):
                for z in range(world.map.size_z()):
                    var type := world.map.get_at_xz(x, z)
                    if type != null:
                        add_conveyor(type, x, z)
        state.BUILDING:
            var state_info: BuildingState = state.get_state_info(State.BUILDING) as BuildingState

            var preview_tiles = {}
            var previews := state_info.previews
            for i in range(previews.size() - 1, -1, -1):
                var preview = previews[i]
                if !preview_tiles.has(preview.position.x):
                    preview_tiles[preview.position.x] = { preview.position.y: { type = preview.type, depth = previews.size() - 1 - i } }
                else:
                    var row = preview_tiles[preview.position.x]
                    if !row.has(preview.position.y):
                        row[preview.position.y] = { type = preview.type, depth = previews.size() - 1 - i }

            for x in range(world.map.size_x()):
                for z in range(world.map.size_z()):
                    var preview = null
                    var row = preview_tiles.get(x)
                    if row != null:
                        var col = row.get(z)
                        if col:
                            preview = col

                    if preview != null:
                        var depth = min(preview.depth, preview_materials.size() - 1)
                        add_conveyor(preview.type, x, z, depth)
                    else:
                        var conveyor = world.map.get_at_xz(x, z)
                        if conveyor != null:
                            add_conveyor(conveyor, x, z)
        state.BUILDING_EDITOR:
            var state_info := state.get_state_info(State.BUILDING_EDITOR) as BuildingEditorState

            var preview_conveyor_map := {
                StateBuildingEditor.BUILD_TYPE.STOP: Stop,
                StateBuildingEditor.BUILD_TYPE.SPAWN: Spawner, #TODO: Change all "spawn" to "SPAWN"
                StateBuildingEditor.BUILD_TYPE.GOAL: Goal,
            }

            var camera := $Camera as Camera
            var mouse_position := get_viewport().get_mouse_position()
            var hovered_position := world.map.pick(camera.project_ray_origin(mouse_position), camera.project_ray_normal(mouse_position))

            var preview_position
            var build_direction
            if state_info.build_from.is_neg_one():
                preview_position = hovered_position
                build_direction = Direction.DOWN
            else:
                preview_position = state_info.build_from
                if preview_position.equals(hovered_position):
                    build_direction = Direction.DOWN
                else:
                    build_direction = preview_position.get_direction_to(hovered_position)
            var preview_conveyor: Base = preview_conveyor_map.get(state_info.build_type).new(build_direction)

            if state_info.build_type == StateBuildingEditor.BUILD_TYPE.SPAWN:
                var outgoing_direction := preview_conveyor.get_directions()
                var outgoing_position := Position.new(preview_position.x, preview_position.y).move_in_direction(outgoing_direction)

                for x in range(world.map.size_x()):
                    for z in range(world.map.size_z()):
                        if preview_position.x == x && preview_position.y == z:
                            add_conveyor(preview_conveyor, x, z, 0)
                        elif outgoing_position.x == x && outgoing_position.y == z:
                            add_conveyor(Stop.new(Direction.flip_direction(outgoing_direction)), x, z, 0)
                        else:
                            var type := world.map.get_at_xz(x, z)
                            if type != null:
                                add_conveyor(type, x, z)
            else:
                for x in range(world.map.size_x()):
                    for z in range(world.map.size_z()):
                        if preview_position.x == x && preview_position.y == z:
                            add_conveyor(preview_conveyor, x, z, 0)
                        else:
                            var type := world.map.get_at_xz(x, z)
                            if type != null:
                                add_conveyor(type, x, z)

func clear_route_preview():
    for arrow in preview_arrows:
        arrow.queue_free()
    preview_arrows.clear()

func update_route_preview():
    clear_route_preview()

    for x in range(world.map.size_x()):
        for z in range(world.map.size_z()):
            var conveyor := world.map.get_at_xz(x, z)
            if conveyor != null && conveyor.is_junction():
                var routing_state := state.get_state_info(State.ROUTING) as RoutingState
                var current_route := router.get_direction(routing_state.path_id,
                                                         x,
                                                         z)
                var outgoing_directions := conveyor.get_directions()
                if Direction.includes(outgoing_directions, Direction.UP):
                    var arrow
                    if current_route == Direction.UP:
                        arrow = ArrowSelected.instance()
                    else:
                        arrow = ArrowUnselected.instance()
                    self.add_child(arrow)
                    preview_arrows.append(arrow)
                    var look_dir := Direction.direction_look_dir(Direction.UP)
                    arrow.look_at(look_dir, Vector3(0.0, 1.0, 0.0))
                    arrow.transform.origin = Vector3(x, 0, -z)
                if Direction.includes(outgoing_directions, Direction.DOWN):
                    var arrow
                    if current_route == Direction.DOWN:
                        arrow = ArrowSelected.instance()
                    else:
                        arrow = ArrowUnselected.instance()
                    self.add_child(arrow)
                    preview_arrows.append(arrow)
                    var look_dir := Direction.direction_look_dir(Direction.DOWN)
                    arrow.look_at(look_dir, Vector3(0.0, 1.0, 0.0))
                    arrow.transform.origin = Vector3(x, 0, -z)
                if Direction.includes(outgoing_directions, Direction.LEFT):
                    var arrow
                    if current_route == Direction.LEFT:
                        arrow = ArrowSelected.instance()
                    else:
                        arrow = ArrowUnselected.instance()
                    self.add_child(arrow)
                    preview_arrows.append(arrow)
                    var look_dir := Direction.direction_look_dir(Direction.LEFT)
                    arrow.look_at(look_dir, Vector3(0.0, 1.0, 0.0))
                    arrow.transform.origin = Vector3(x, 0, -z)
                if Direction.includes(outgoing_directions, Direction.RIGHT):
                    var arrow
                    if current_route == Direction.RIGHT:
                        arrow = ArrowSelected.instance()
                    else:
                        arrow = ArrowUnselected.instance()
                    self.add_child(arrow)
                    preview_arrows.append(arrow)
                    var look_dir := Direction.direction_look_dir(Direction.RIGHT)
                    arrow.look_at(look_dir, Vector3(0.0, 1.0, 0.0))
                    arrow.transform.origin = Vector3(x, 0, -z)

var _conveyor_debug: bool = false
var _cargo_debug: bool = false
var _route_debug: bool = false
var _tile_debug: bool = false

var _debug_conveyor_windows := []
func debug_conveyor():
    for window in _debug_conveyor_windows:
        self.remove_child(window)
        window.queue_free()
    _debug_conveyor_windows.clear()

    if !_conveyor_debug:
        return

    for z in range(world.map.size_z()):
        for x in range(world.map.size_x()):
            var conveyor := world.map.get_at_xz(x, z)
            if conveyor != null:
                var new_window := ConveyorDebug.instance()
                _debug_conveyor_windows.append(new_window)
                add_child(new_window)
                new_window.find_node("Type").text = conveyor.get_type_string()
                new_window.find_node("State").text = conveyor.state_to_string(conveyor._state)
                new_window.find_node("TransStep").text = str(conveyor._transition_step)
                new_window.find_node("TransIn").text = Direction.direction_to_string(conveyor._transition_incoming)
                new_window.find_node("TransOut").text = Direction.direction_to_string(conveyor._transition_outgoing)
                new_window.find_node("Incoming").text = Direction.direction_to_string(conveyor._possible_incoming_directions)
                new_window.find_node("Outgoing").text = Direction.direction_to_string(conveyor._possible_outgoing_directions)

                new_window.rect_position = $Camera.unproject_position(Vector3(x, 0, -z)) - Vector2(new_window.rect_size.x / 2.0, 0)

var _debug_cargo_windows := []
func debug_cargo():
    for window in _debug_cargo_windows:
        self.remove_child(window)
        window.queue_free()
    _debug_cargo_windows.clear()

    if !_cargo_debug:
        return

    for cargo in world.cargoes:
        var new_window := CargoDebug.instance()
        _debug_cargo_windows.append(new_window)
        add_child(new_window)
        new_window.rect_position = $Camera.unproject_position(Vector3(cargo.position.x - 0.5, 0, -cargo.position.y))

        new_window.find_node("Step").text = str(cargo.distance_from_center)
        new_window.find_node("Side").text = Direction.direction_to_string(cargo.side_of_conveyor)

func create_counter(value: int) -> MeshInstance:
    var mesh_instance := MeshInstance.new()
    mesh_instance.set_mesh(PlaneMesh.new())
    mesh_instance.cast_shadow = false

    var viewport := Viewport.new()
    viewport.size = Vector2(26, 14)
    viewport.transparent_bg = true
    viewport.hdr = false
    viewport.usage = viewport.USAGE_2D_NO_SAMPLING
    viewport.render_target_v_flip = true
    viewport.disable_3d = true
    mesh_instance.add_child(viewport)

    var counter := Label.new()
    counter.text = str(value)
    counter.align = counter.ALIGN_LEFT
    counter.name = "counter"
    viewport.add_child(counter)

    var counter_material = SpatialMaterial.new()
    counter_material.flags_unshaded = true
    counter_material.flags_do_not_receive_shadows = true
    counter_material.albedo_texture = viewport.get_texture()
    counter_material.resource_local_to_scene = true
    counter_material.flags_no_depth_test = true
    counter_material.render_priority = counter_material.RENDER_PRIORITY_MAX
    mesh_instance.set_surface_material(0, counter_material)

    return mesh_instance


var plane = PlaneMesh.new()

func create_position(position: Position) -> MeshInstance:
    var mesh_instance := MeshInstance.new()
    mesh_instance.set_mesh(plane)
    mesh_instance.cast_shadow = false

    var viewport := Viewport.new()
    viewport.size = Vector2(26 * 2, 14 * 2)
    viewport.transparent_bg = true
    viewport.hdr = false
    viewport.usage = viewport.USAGE_2D_NO_SAMPLING
    viewport.render_target_v_flip = true
    viewport.disable_3d = true
    mesh_instance.add_child(viewport)

    var counter := Label.new()
    counter.text = "%d, %d" % [position.x, position.y]
    counter.align = counter.ALIGN_LEFT
    counter.name = "counter"
    viewport.add_child(counter)

    var counter_material = SpatialMaterial.new()
    counter_material.flags_unshaded = true
    counter_material.flags_do_not_receive_shadows = true
    counter_material.albedo_texture = viewport.get_texture()
    counter_material.resource_local_to_scene = true
    counter_material.flags_no_depth_test = true
    counter_material.render_priority = counter_material.RENDER_PRIORITY_MAX
    mesh_instance.set_surface_material(0, counter_material)

    return mesh_instance

var route_shapes := []
func debug_routes():
    if !_route_debug:
        for shape in route_shapes:
            shape.queue_free()
        route_shapes.clear()
        return

    var count = 0
    for small_id in router.cargo_in_route.keys():
        for big_id in router.cargo_in_route[small_id].keys():
            count += 1

    if count != route_shapes.size():
        for shape in route_shapes:
            shape.queue_free()
        route_shapes.clear()

        var material = SpatialMaterial.new()

        material.flags_transparent = true
        material.flags_do_not_receive_shadows = true
        material.albedo_color = Color("aa222222")

        for small_id in router.cargo_in_route.keys():
            for big_id in router.cargo_in_route[small_id].keys():
                var sp = router.decode_id(small_id)
                var bp = router.decode_id(big_id)

                var min_x = min(sp.x, bp.x) - 0.25
                var min_y = min(sp.y, bp.y) - 0.25

                var max_x = max(sp.x, bp.x) + 0.25
                var max_y = max(sp.y, bp.y) + 0.25

                var counter := create_counter(router.cargo_in_route[small_id][big_id].cargo_count)
                counter.translate(Vector3((min_x + max_x) / 2.0, 0.25, (-min_y + -max_y) / 2.0))
                counter.scale = Vector3(.25, 1, .25)

                var line := Line2D.new()
                line.add_point($Camera.unproject_position(Vector3(sp.x, 0, -sp.y)))
                line.add_point($Camera.unproject_position(Vector3(bp.x, 0, -bp.y)))
                line.width = 10
                line.show_behind_parent = true
                line.default_color = Color("aa222222")
                line.name = "line"

                counter.add_child(line)

                self.add_child(counter)
                route_shapes.append(counter)
    else:
        var counter = 0
        for small_id in router.cargo_in_route.keys():
            for big_id in router.cargo_in_route[small_id].keys():
                var new_count = router.cargo_in_route[small_id][big_id].cargo_count

                var sp = router.decode_id(small_id)
                var bp = router.decode_id(big_id)

                var mesh_instance := route_shapes[counter] as MeshInstance
                mesh_instance.find_node("counter", true, false).text = str(new_count)

                var line = mesh_instance.find_node("line", true, false)
                line.set_point_position(0, $Camera.unproject_position(Vector3(sp.x, 0, -sp.y)))
                line.set_point_position(1, $Camera.unproject_position(Vector3(bp.x, 0, -bp.y)))

                counter += 1

const tile_debug_size = 9

var tile_labels = []
func debug_tiles():
    if !_tile_debug:
        for label in tile_labels:
            label.queue_free()
        tile_labels.clear()
        return

    if tile_labels.empty():
        var material = SpatialMaterial.new()

        material.flags_transparent = true
        material.flags_do_not_receive_shadows = true
        material.albedo_color = Color("aa222222")

        for i in range(0, tile_debug_size * tile_debug_size):
            var counter := create_position(Position.new(0, 0))
            self.add_child(counter)
            tile_labels.append(counter)
    else:
        var forward := -($Camera as Camera).global_transform.basis.z
        var position := ($Camera as Camera).global_transform.origin

        var center = world.map.pick(position, forward)

        var i := 0
        for z in range((center.y - tile_debug_size / 2), (center.y + tile_debug_size / 2) + 1):
            for x in range((center.x - tile_debug_size / 2), (center.x + tile_debug_size / 2) + 1):
                var mesh_instance = tile_labels[i] as MeshInstance
                mesh_instance.find_node("counter", true, false).text = "%d, %d" % [x, z]
                mesh_instance.transform.origin = Vector3(x, 0.0, -z)
                mesh_instance.scale = Vector3(.25, 1, .25)

                i += 1

func update_time_controls(simulation_state: int) -> void:
    var controls := self.find_node("TimeControls")
    if simulation_state == SimulationState.RUN:
        controls.find_node("Stop").show()
        controls.find_node("Start").hide()
        controls.find_node("Step").hide()
    else:
        controls.find_node("Stop").hide()
        controls.find_node("Start").show()
        controls.find_node("Step").show()

func _on_Stop_pressed():
    _simulation_state = SimulationState.STOP
    update_time_controls(SimulationState.STOP)

func _on_Start_pressed():
    _simulation_state = SimulationState.RUN
    update_time_controls(SimulationState.RUN)

func _on_Step_pressed():
    _simulation_state = SimulationState.STEP

var save_dialog: FileDialog
var load_dialog: FileDialog

func _on_File_pressed(id):
    if id == 0:
        create_new_scenario(WORLD_SIZE_X, WORLD_SIZE_Z)
    elif id == 1:
        if load_dialog == null:
            load_dialog = FileDialog.new()
            load_dialog.mode = FileDialog.MODE_OPEN_ANY
            load_dialog.add_filter("*.txt; Test scenario files")
            load_dialog.connect("file_selected", FileUtil, "load_test_scenario", [self])
            add_child(load_dialog)

        load_dialog.current_dir = "res://script_tests/test_game_scenarios"


        load_dialog.popup_centered(Vector2(600, 600))
    elif id == 2:
        if save_dialog == null:
            save_dialog = FileDialog.new()
            save_dialog.mode = FileDialog.MODE_SAVE_FILE
            save_dialog.add_filter("*.txt; Test scenario files")
            add_child(save_dialog)

        if save_dialog.is_connected("file_selected", FileUtil, "save_test_scenario"):
            save_dialog.disconnect("file_selected", FileUtil, "save_test_scenario")
        save_dialog.connect("file_selected", FileUtil, "save_test_scenario", [world, router])

        save_dialog.current_dir = "res://script_tests/test_game_scenarios"
        save_dialog.popup_centered(Vector2(600, 600))
    elif id == 3:
        FileUtil.save_default_scenario(world, router)
    elif id == 4:
        if FileUtil.default_scenario_exists():
            FileUtil.load_default_scenario(self)

func _on_Debug_pressed(id):
    pass
    if id == 0:
        _conveyor_debug = !_conveyor_debug
        $VBoxContainer/MenuBar.find_node("Debug").get_popup().set_item_checked(id, _conveyor_debug)
    elif id == 1:
        _cargo_debug = !_cargo_debug
        $VBoxContainer/MenuBar.find_node("Debug").get_popup().set_item_checked(id, _cargo_debug)
    elif id == 2:
        _route_debug = !_route_debug
        $VBoxContainer/MenuBar.find_node("Debug").get_popup().set_item_checked(id, _route_debug)
    elif id == 3:
        _tile_debug = !_tile_debug
        $VBoxContainer/MenuBar.find_node("Debug").get_popup().set_item_checked(id, _tile_debug)

func _on_Editor_toggled(value: bool):
    (find_node('ConveyorPickerBackground') as PanelContainer).visible = value

    if !value:
        update_conveyors();
