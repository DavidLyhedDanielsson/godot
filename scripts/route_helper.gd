extends Node

const Cargo = preload("res://scripts/cargo.gd")
const Router = preload("res://scripts/router.gd")
const Position = preload("res://scripts/position.gd")
const Array2D = preload("res://scripts/array_2d.gd")
const Base = preload("res://scripts/conveyors/base.gd")
const Map = preload("res://scripts/map.gd")

const RouteWalker = preload("res://scripts/route_walker.gd")

static func split_route(map: Map, router: Router, start_x: int, start_y: int, end_x: int, end_y: int, split: Position) -> void:
    var cargo_in_route: Array = router.get_cargo_in_route(start_x, start_y, end_x, end_y)
    if cargo_in_route.empty():
        return

    var split_at := map.try_get_at(split)
    var directions = split_at._directions

    var lhs_direction
    if Direction.includes(directions, Direction.UP):
        lhs_direction = Direction.UP
    elif Direction.includes(directions, Direction.DOWN):
        lhs_direction = Direction.DOWN
    elif Direction.includes(directions, Direction.LEFT):
        lhs_direction = Direction.LEFT
    # If it does not contain up, down, or left, the conveyor contains only 1
    # direction and cannot be split. The route should be extended in that case

    var rhs_direction = ~lhs_direction & directions

    assert(lhs_direction != Direction.NONE)
    assert(rhs_direction != Direction.NONE)

    var lhs_route: Array = Util.collect(RouteWalker.new(map, split, lhs_direction))
    var lhs_end = lhs_route.back()
    for i in range(cargo_in_route.size()):
        var box = cargo_in_route[i]
        for pos in lhs_route:
            if box.position.equals(pos):
                router.exit_route(box, start_x, start_y, end_x, end_y)
                router.enter_route(box, lhs_end.x, lhs_end.y, split.x, split.y)
                cargo_in_route.remove(i)
                i -= 1

    var rhs_route: Array = Util.collect(RouteWalker.new(map, split, lhs_direction))
    var rhs_end = rhs_route.back()
    for i in range(cargo_in_route.size()):
        var box = cargo_in_route[i]
        for pos in rhs_route:
            if box.position.equals(pos):
                router.exit_route(box, start_x, start_y, end_x, end_y)
                router.enter_route(box, rhs_end.x, rhs_end.y, split.x, split.y)
                cargo_in_route.remove(i)
                i -= 1

    for box in cargo_in_route:
        router.exit_route(box, start_x, start_y, end_x, end_y)
        if box.movement_direction == lhs_direction:
            router.enter_route(box, lhs_end.x, lhs_end.y, split.x, split.y)
        else:
            router.enter_route(box, rhs_end.x, rhs_end.y, split.x, split.y)

static func collect_route(map: Map, position: Position) -> Array:
    position = Position.new(position.x, position.y)
    var conveyor := map.try_get_at(position)
    var directions = conveyor._directions
    assert(Direction.count_directions(directions) <= 2)

    var lhs_direction
    if Direction.includes(directions, Direction.UP):
        lhs_direction = Direction.UP
    elif Direction.includes(directions, Direction.DOWN):
        lhs_direction = Direction.DOWN
    elif Direction.includes(directions, Direction.LEFT):
        lhs_direction = Direction.LEFT
    # If it does not contain up, down, or left, the conveyor contains only 1
    # direction and cannot be split. The route should be extended in that case

    var rhs_direction = ~lhs_direction & directions

    # Skip the first conveyor at `position` since it will be added twice
    # otherwise
    var lhs = Util.reverse(Util.collect(RouteWalker.new(map, position, lhs_direction).skip()))
    var result = lhs + Util.collect(RouteWalker.new(map, position, rhs_direction))
    return result

static func get_outgoing_direction(map: Map, router: Router, path_id: int, cargo_outgoing_direction: int, conveyor_x: int , conveyor_z: int) -> int:
    var conveyor := map.try_get_at_xz(conveyor_x, conveyor_z)
    if conveyor == null:
        return Direction.NONE

    if conveyor.is_junction():
        # the direction should have been set earlier since the box is now on
        # this junction
        var next_outgoing_direction := router.get_direction(path_id, conveyor_x, conveyor_z)
        if next_outgoing_direction != Direction.NONE:
            return next_outgoing_direction

        var directions := conveyor.get_directions()
        if Direction.includes(directions, cargo_outgoing_direction):
            return cargo_outgoing_direction
        else:
            var route_rotation := [ Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT ]
            # If the cargo is at a junction where it cannot continue in the
            # direction it is currently travelling, that means any of the
            # other three directions work.
            var new_route_index := (route_rotation.find(cargo_outgoing_direction) + 1) % route_rotation.size()
            return route_rotation[new_route_index]
    else:
        var directions := conveyor.get_directions()
        return directions & ~(directions & Direction.flip_direction(cargo_outgoing_direction))
