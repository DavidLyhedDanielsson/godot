const ConveyorType = preload("res://scripts/conveyor_type.gd").ConveyorType
const StateBuilding = preload("res://scripts/states/building.gd")
const StateBuildingEditor = preload("res://scripts/states/building_editor.gd")
const State = preload("res://scripts/state.gd")
const Position = preload("res://scripts/position.gd")
const ConveyorFactory = preload("res://scripts/conveyor_factory.gd")

const Stop = preload("res://scripts/conveyors/stop.gd")
const Spawner = preload("res://scripts/conveyors/spawner.gd")
const Goal = preload("res://scripts/conveyors/goal.gd")

static func input_none(event, camera, game) -> void:
    if event is InputEventMouseButton and event.pressed:
        var state = game.state
        var world = game.world

        var from = camera.project_ray_origin(event.position)
        var dir = camera.project_ray_normal(event.position)
        var hit = world.map.pick(from, dir)

        if !world.map.is_valid(hit.x, hit.y):
            return

        var hit_conveyor = world.map.get_at(hit)
        if hit_conveyor == null:
            return
        if event.button_index == 1:
            if(!hit_conveyor.is_spawner()):
                state.set_building()
                state.get_state_info(State.BUILDING).build_from = hit
                state.get_state_info(State.BUILDING).first_conveyor = hit_conveyor
                state.get_state_info(State.BUILDING).previews.append({position = hit, type = hit_conveyor})
                game.update_conveyors()
            else:
                state.set_routing()
                state.get_state_info(State.ROUTING).path_id = game.router.create_id(hit.x, hit.y)
                state.get_state_info(State.ROUTING).route_from = hit
                game.update_route_preview()
        elif event.button_index == 2:
            world.remove_conveyor(hit)
            game.router.delete_route(hit.x, hit.y)
            game.update_conveyors()


static func input_building(event, camera, game) -> void:
    var state = game.state
    var world = game.world

    var state_info: StateBuilding = state.get_state_info(State.BUILDING)
    if event is InputEventMouseButton and event.pressed and event.button_index == 1:
        var from = camera.project_ray_origin(event.position)
        var dir = camera.project_ray_normal(event.position)
        var hit = world.map.pick(from, dir)

        for preview in state_info.previews:
            var at = world.map.get_at(preview.position)
            if at != null && Direction.count_directions(at._directions) <= 2 && (Direction.count_directions(preview.type._directions) > 2):
                var route: Array = RouteHelper.collect_route(world.map, preview.position)
                RouteHelper.split_route(world.map, game.router, route.front().x, route.front().y, route.back().x, route.back().y, preview.position)

            world.map.set_at(preview.position.x, preview.position.y, preview.type)

        state.set_none()
        game.update_conveyors()
    elif event is InputEventMouseMotion:
        var mouse_position = event.position

        var to = world.map.pick(camera.project_ray_origin(mouse_position), camera.project_ray_normal(mouse_position))
        if !world.map.is_valid(to.x, to.y):
            return

        if state_info.build_from.is_next_to(to):
            var build_direction = state_info.build_from.get_direction_to(to)

            var from_type
            var from_preview = state_info.get_preview_at_position(state_info.build_from)
            if from_preview == null:
                from_type = world.map.try_get_at(state_info.build_from)
            else:
                from_type = from_preview.type

            var to_preview = state_info.get_preview_at_position(to)

            var to_type
            if to_preview == null:
                to_type = world.map.try_get_at(to)
            else:
                to_type = to_preview.type

            if state_info.previews.size() >= 2 && state_info.previews[-2].position.equals(to):
                if state_info.previews.size() > 2:
                    # state_info.previews.pop_back()
                    # var above = state_info.get_preview_at_position(Position.new(to.x, to.y + 1))
                    # var below = state_info.get_preview_at_position(Position.new(to.x, to.y - 1))
                    # var left = state_info.get_preview_at_position(Position.new(to.x - 1, to.y))
                    # var right = state_info.get_preview_at_position(Position.new(to.x + 1, to.y))
                    # var above_type = world.map.try_get_at(to.x, to.y + 1) if above == null else above.type
                    # var below_type = world.map.try_get_at(to.x, to.y - 1) if below == null else below.type
                    # var left_type = world.map.try_get_at(to.x - 1, to.y) if left == null else left.type
                    # var right_type = world.map.try_get_at(to.x + 1, to.y) if right == null else right.type
                    #var new_type = ConveyorPlacer.get_new_parent_type(above_type, below_type, left_type, right_type, Direction.NONE)
                    #state_info.previews.back().type = new_type
                    push_error("TODO")
                else:
                    state_info.previews.pop_back()
                    state_info.previews.back().type = state_info.first_type

                state_info.build_from = to
                game.update_conveyors()
            elif Direction.includes(from_type.get_allowed_build_directions(), build_direction) \
                    && (to_type == null || Direction.includes(to_type.get_allowed_build_directions(), Direction.flip_direction(build_direction))):
                var preview_type
                if to_type == null || !to_type.is_static():
                    preview_type = ConveyorFactory.add_direction_to(to_type, Direction.flip_direction(build_direction))
                else:
                    preview_type = to_type

                if from_preview != null:
                    var new_parent = ConveyorFactory.add_direction_to(from_preview.type, build_direction)
                    from_preview.type = new_parent

                state_info.previews.append({position = to, type = preview_type})
                state_info.build_from = to

                game.update_conveyors()

static func input_building_editor(event, camera, game) -> void:
    var state = game.state
    var world = game.world

    var state_info = state.get_state_info(State.BUILDING_EDITOR)
    if event is InputEventMouseButton and event.button_index == 1:
        if event.pressed:
            var from = camera.project_ray_origin(event.position)
            var dir = camera.project_ray_normal(event.position)
            var hit = world.map.pick(from, dir)
            if !world.map.is_valid(hit.x, hit.y):
                return

            state_info.build_from = hit
            state_info.mouse_down = true
        elif state_info.mouse_down:
            var mouse_position = event.position
            var hovered_position = world.map.pick(camera.project_ray_origin(mouse_position), camera.project_ray_normal(mouse_position))
            var build_direction = state_info.build_from.get_direction_to(hovered_position)
            if build_direction == Direction.NONE:
                build_direction = Direction.DOWN

            if state_info.build_type == StateBuildingEditor.BUILD_TYPE.SPAWN:
                world.add_spawner(game.router, state_info.build_from.x, state_info.build_from.y, build_direction)
                var outgoing_direction = world.map.get_at(state_info.build_from).get_directions()
                var outgoing_position = Position.new(state_info.build_from.x, state_info.build_from.y).move_in_direction(outgoing_direction)
                world.map.set_at(outgoing_position.x, outgoing_position.y, Stop.new(Direction.flip_direction(outgoing_direction)))
            else:
                var new_conveyor
                if state_info.build_type == StateBuildingEditor.BUILD_TYPE.STOP:
                    new_conveyor = Stop.new(build_direction)
                else:
                    new_conveyor = Goal.new(build_direction)
                world.map.set_at(state_info.build_from.x, state_info.build_from.y, new_conveyor)

            state_info.mouse_down = false
            state_info.build_from = Position.new(-1, -1)

            game.update_conveyors()
    elif event is InputEventMouseMotion:
        game.update_conveyors();

static func input_routing(event, camera, game):
    var world = game.world
    var state = game.state
    var router = game.router

    if event is InputEventMouseButton and event.pressed and event.button_index == 1:
        var from = camera.project_ray_origin(event.position)
        var dir = camera.project_ray_normal(event.position)
        var hit_position = world.map.pick(from, dir)
        if !world.map.is_valid(hit_position.x, hit_position.y):
            return

        var conveyor = world.map.get_at(hit_position)
        if conveyor == null:
            return

        var routing_state = state.get_state_info(State.ROUTING)
        if hit_position.x == routing_state.route_from.x && hit_position.y == routing_state.route_from.y:
            state.set_none()
            for arrow in game.preview_arrows:
                arrow.queue_free()
            game.preview_arrows.clear()
        else:
            var current_route = router.get_direction(routing_state.path_id,
                                                     hit_position.x,
                                                     hit_position.y)
            var possible_directions = conveyor.get_directions()
            var route_rotation = [ Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT ]
            var current_route_index = route_rotation.find(current_route)

            var start_index = current_route_index
            # It will never rotate back to -1
            if start_index == -1:
                start_index = route_rotation.size() - 1
            current_route_index = (current_route_index + 1) % route_rotation.size()
            while route_rotation[current_route_index] & possible_directions == 0:
                current_route_index = (current_route_index + 1) % route_rotation.size()
                # Safety check to avoid infinite loop
                if current_route_index == start_index:
                    break

            router.set_direction(routing_state.path_id,
                                 hit_position.x,
                                 hit_position.y,
                                 route_rotation[current_route_index])
            game.update_route_preview()
