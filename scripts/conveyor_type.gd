enum ConveyorType {
    CORNER = 0,
    CROSS = 1,
    GOAL = 2,
    JUNCTION = 3,
    SPAWNER = 4,
    STOP = 5,
    STRAIGHT = 6,
}
