extends Node

# No enum to avoid having to type `Direction.Direction.UP` when referring to
# this as a singleton. There's no "enum as a type" concept in gdscript anyway :(
const UP = 1
const DOWN = 2
const LEFT = 4
const RIGHT = 8
const ALL = 15
const NONE = 0

# @param {Direction} dir
# @returns {String}
static func direction_to_string(dir: int) -> String:
    match dir:
        NONE: return "None"
        UP: return "Up"
        DOWN: return "Down"
        LEFT: return "Left"
        RIGHT: return "Right"
        _:
            var result: = ""
            if dir & UP != 0:
                result += ", UP"
            if dir & DOWN != 0:
                result += ", DOWN"
            if dir & LEFT != 0:
                result += ", LEFT"
            if dir & RIGHT != 0:
                result += ", RIGHT"
            if result.length() == 0:
                push_error('No such direction, value is ' + str(dir))
                result = "Unknown"
            else:
                result.erase(0, 2)

            return result

static func flip_direction(direction: int) -> int:
    match direction:
        UP: return DOWN
        DOWN: return UP
        LEFT: return RIGHT
        RIGHT: return LEFT
        _: return NONE

# Converts a direction to a "look direction" vector
#
# @param {Direction} direction
# @returns {Vector3}
static func direction_look_dir(direction: int) -> Vector3:
    match direction:
        UP:
            return Vector3(0, 0, -1)
        DOWN:
            return Vector3(0, 0, 1)
        LEFT:
            return Vector3(-1, 0, 0)
        RIGHT:
            return Vector3(1, 0, 0)
        _ :
            return Vector3(0, 0, 0)

static func includes(direction, includes) -> bool:
    return (direction & includes != 0) || includes == NONE

static func count_directions(directions: int) -> int:
    return int(includes(directions, UP))\
            + int(includes(directions, DOWN))\
            + int(includes(directions, LEFT))\
            + int(includes(directions, RIGHT))
