# Custom iterator to walk a route and get all conveyor positions from that
# route. The iterator will include the final junction/stop/goal

# See path_walker for an explanation of path vs route

const Position = preload("res://scripts/position.gd")
const Map = preload("res://scripts/map.gd")

const Base = preload("res://scripts/conveyors/base.gd")

# Hungarian notation due to lack of language features :(
var c_map: Map
var c_start_position: Position
var c_start_direction: int
# mut
var _position: Position
var _direction: int
var _end_found: bool

func _init(map: Map, position: Position, direction: int):
    self.c_map = map
    self.c_start_position = Position.new(position.x, position.y)
    self.c_start_direction = direction

    _iter_init(self)

func _iter_init(_self) -> bool:
    self._position = Position.new(c_start_position.x, c_start_position.y)
    self._direction = c_start_direction
    self._end_found = false
    return c_map.try_get_at(_position) != null

func _iter_next(_self) -> bool:
    # When a junction, stop, spawner, etc is found it should be included in the
    # iterator, so the method must return true. After that it should return
    # false. _end_found makes sure this is what happens

    if _end_found:
        return false

    _position.move_in_direction(_direction)

    var conveyor := c_map.try_get_at(_position)
    # This case is possible if there is no conveyor at _start_position +
    # _direction
    if conveyor == null:
        return false

    if Direction.count_directions(conveyor.get_directions()) != 2:
        _end_found = true
    else:
        var directions := conveyor.get_directions()
        _direction = directions & ~Direction.flip_direction(_direction)

    return true

func _iter_get(_self) -> Position:
    return Position.new(_position.x, _position.y)

# get is reserved
func current() -> Position:
    return Position.new(_position.x, _position.y)

func skip() -> Object:
    _iter_next(self)
    return self

func conveyor() -> Base:
    return c_map.try_get_at(_position)
