const Position = preload("res://scripts/position.gd")

const ConveyorFactory = preload("res://scripts/conveyor_factory.gd")

const Cargo = preload("res://scripts/cargo.gd")
const Router = preload("res://scripts/router.gd")
const Map = preload("res://scripts/map.gd")
const RouteHelper = preload("res://scripts/route_helper.gd")

const TimeConstants = preload("res://scripts/time_constants.gd")

const Base = preload("res://scripts/conveyors/base.gd")
const Spawner = preload("res://scripts/conveyors/spawner.gd")

const RouteWalker = preload("res://scripts/route_walker.gd")
const PathWalker = preload("res://scripts/path_walker.gd")

var cargoes := []
var map: Map

# Initializes the world grid.
#
# @param {int} world_size_x - world size in the x-axis
# @param {int} world_size_z - world size in the z-axis
func _init(world_size_x: int, world_size_z: int) -> void:
    map = Map.new(world_size_x, world_size_z)

func update_conveyors(router: Router) -> bool:
    for z in range(map.size_z()):
        for x in range(map.size_x()):
            var conveyor := map.get_at_xz(x, z)
            if conveyor != null:
                conveyor.update()
                if conveyor.is_spawner() && conveyor.ready_to_spawn():
                    conveyor.spawn()
                    force_spawn(router, x, z)

    var return_value := false
    var i := 0
    while i < cargoes.size():
        var box: Cargo = cargoes[i]
        var conveyor := map.try_get_at(box.position)
        if conveyor == null:
            DebugInformation.print("Invalid conveyor position ", box.position.x, "; ", box.position.y)
            continue

        if conveyor.move_cargo(router, box) == Base.IncomingOutgoing.INCOMING:
            if conveyor.get_possible_outgoing_directions() == Direction.NONE && !conveyor.is_goal() && box.distance_from_center <= 60:
                stop_conveyor(box.position.x, box.position.y)

        if box_has_crossed_center(box):
            if conveyor.is_goal():
                exit_route(router, box, box.position, Direction.flip_direction(box.movement_direction))
                Util.swap_and_remove(cargoes, i)
                i -= 1
                box.queue_free()
            else:
                box.last_side_of_conveyor = box.side_of_conveyor
                box.side_of_conveyor = RouteHelper.get_outgoing_direction(map, router, box.path_id, box.movement_direction, box.position.x, box.position.y)

                if conveyor.is_junction():
                    exit_route(router, box, box.position, Direction.flip_direction(box.movement_direction))
        elif box_has_crossed_edge(box):
            move_cargo_to_next_conveyor(box)

            var walker: PathWalker = PathWalker.new(map, router, box.path_id, box.position, box.movement_direction).skip()

            var next_conveyor := walker.conveyor()
            if next_conveyor.is_junction():
                change_route_direction_for_cargo(next_conveyor, box, router)
                enter_route(router, box, walker.current(), walker.next_direction())
        i += 1

    return return_value

func box_has_crossed_center(box: Cargo) -> bool:
    return box.distance_from_center <= 0

func box_has_crossed_edge(box: Cargo) -> bool:
    # 120 steps gives an interval of [0, 120)
    return box.distance_from_center >= TimeConstants.CARGO_STEPS - 1 && box.side_of_conveyor == box.movement_direction

func change_route_direction(x: int, z: int, direction: int) -> void:
    var position := Position.new(x, z).move_in_direction(direction)

    var conveyor := map.try_get_at(position)

    while conveyor != null:
        # Junction directions are controlled by the router later
        if conveyor.is_junction():
            break

        var incoming_direction := Direction.flip_direction(direction)
        var outgoing_direction := conveyor.get_directions() & ~incoming_direction
        conveyor.change_direction(incoming_direction, outgoing_direction)
        direction = outgoing_direction

        # Unlike the junction, these should still have their direction set
        if conveyor.is_spawner() || conveyor.is_goal() || conveyor.is_stop():
            break

        position.move_in_direction(direction)
        conveyor = map.try_get_at(position)

func move_cargo_to_next_conveyor(box: Cargo) -> void:
    var new_position := Position.new(box.position.x, box.position.y).move_in_direction(box.side_of_conveyor)

    var new_conveyor := map.get_at(new_position)
    if new_conveyor == null:
        return

    box.position = new_position
    box.last_side_of_conveyor = box.side_of_conveyor
    box.side_of_conveyor = Direction.flip_direction(box.side_of_conveyor)
    box.distance_from_center = TimeConstants.CARGO_STEPS - (box.distance_from_center - TimeConstants.CARGO_STEPS)

func change_route_direction_for_cargo(junction: Base, box: Cargo, router: Router) -> void:
    var walker := PathWalker.new(map, router, box.path_id, box.position, box.movement_direction).skip()
    var next_position: Position = walker.current()

    junction.change_direction(Direction.flip_direction(walker.last_direction()), walker.next_direction())
    change_route_direction(next_position.x, next_position.y, walker.next_direction())

func remove_conveyor(position: Position) -> void:
    map.set_at(position.x, position.y, null)
    var up = map.try_get_at_xz(position.x, position.y + 1)
    if up:
        map.set_at(position.x, position.y + 1, ConveyorFactory.remove_direction_from(up, Direction.DOWN))

    var down = map.try_get_at_xz(position.x, position.y - 1)
    if down:
        map.set_at(position.x, position.y - 1, ConveyorFactory.remove_direction_from(down, Direction.UP))

    var left = map.try_get_at_xz(position.x - 1, position.y)
    if left:
        map.set_at(position.x - 1, position.y, ConveyorFactory.remove_direction_from(left, Direction.RIGHT))

    var right = map.try_get_at_xz(position.x + 1, position.y)
    if right:
        map.set_at(position.x + 1, position.y, ConveyorFactory.remove_direction_from(right, Direction.LEFT))

func enter_route(router: Router, box: Cargo, position: Position, direction: int) -> void:
    position = Position.new(position.x, position.y)
    var end: Position = Util.last(RouteWalker.new(map, position, direction))
    router.enter_route(box, position.x, position.y, end.x, end.y)

func exit_route(router: Router, box: Cargo, position: Position, direction: int) -> void:
    position = Position.new(position.x, position.y)
    var end: Position = Util.last(RouteWalker.new(map, position, direction))
    router.exit_route(box, position.x, position.y, end.x, end.y)

# Add a spawner at the given location pointing in the given direction
#
# @param {int} x - x-position of the new spawner
# @param {int} z - z-position of the new spawner
# @returns {bool} whether or not the placement succeeded
func add_spawner(router: Router, x: int, z: int, direction: int) -> bool:
    if !map.is_valid(x, z):
        return false

    var new_type := Spawner.new(direction)

    map.set_at(x, z, new_type)
    router.create_route(x, z)

    return true

func force_spawn(router: Router, x: int, z: int) -> bool:
    var spawner := map.try_get_at_xz(x, z)
    if spawner == null || !spawner.is_spawner():
        return false

    var path_id := router.create_id(x, z)

    var direction := spawner.get_directions()
    var box := Cargo.new(path_id, x, z, direction)
    cargoes.append(box)

    spawner.force_running(Direction.NONE, spawner.get_directions())
    var walker = PathWalker.new(map, router, path_id, Position.new(x, z), spawner.get_directions()).skip()
    var next_conveyor = walker.conveyor()
    if next_conveyor.is_junction():
        next_conveyor.change_direction(Direction.flip_direction(walker.last_direction()), walker.next_direction())
        enter_route(router, box, walker.current(), walker.next_direction())
        change_route_direction(walker.current().x, walker.current().y, walker.next_direction())

    change_route_direction(x, z, spawner.get_directions())

    enter_route(router, box, Position.new(x, z), spawner.get_directions())

    return true

func stop_conveyor(x: int, z: int) -> void:
    var position := Position.new(x, z)
    var conveyor := map.try_get_at(position)
    if conveyor == null:
        return

    assert(conveyor.is_stop())

    # Will only work STOP types
    var direction := conveyor.get_directions()

    while conveyor != null:
        conveyor.change_direction(Direction.NONE, Direction.NONE)

        if conveyor.is_spawner() || conveyor.is_goal():
            break

        # TODO: What about corners?
        position.move_in_direction(direction)
        conveyor = map.try_get_at(position)

        if conveyor.is_junction():
            break

