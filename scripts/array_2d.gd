var _rows: = []

var _size_x: = 0
var _size_y: = 0

func _init(size_x: int, size_y: int) -> void:
    _size_x = size_x
    _size_y = size_y

    _rows.resize(size_y)
    for y in range(size_y):
        var row = []
        row.resize(size_x)
        for x in range(size_x):
            row[x] = null
        _rows[y] = row

# "set" is already a function so can't use that as a name, and I do not want
# to use anything longer than "set"
func s(x: int, y: int, value) -> void:
    _rows[y][x] = value

# Same issue as with "set"
func g(x: int, y: int):
    return _rows[y][x]

func try_g(x: int, y: int):
    if x < 0 || x >= _size_x:
        return null
    if y < 0 || y >= _size_y:
        return null
    return _rows[y][x]

func size_x() -> int:
    return _size_x

func size_y() -> int:
    return _size_y
