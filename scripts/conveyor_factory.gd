const Base = preload("conveyors/base.gd")
const Corner = preload("conveyors/corner.gd")
const Cross = preload("conveyors/cross.gd")
const Goal = preload("conveyors/goal.gd")
const Junction = preload("conveyors/junction.gd")
const Spawner = preload("conveyors/spawner.gd")
const Stop = preload("conveyors/stop.gd")
const Straight = preload("conveyors/straight.gd")

const ConveyorType = preload("res://scripts/conveyor_type.gd").ConveyorType

static func add_direction_to(conveyor, direction) -> Object:
    if conveyor == null:
        return Stop.new(direction)

    var conveyor_type = conveyor.get_type()
    if conveyor_type == ConveyorType.GOAL \
        || conveyor_type == ConveyorType.SPAWNER:
        return null # TODO: Duplicate instead

    var new_directions = conveyor.get_directions() | direction
    var direction_count = Direction.count_directions(new_directions)

    if direction_count == 1:
        return Stop.new(new_directions)
    elif direction_count == 2:
        if new_directions == (Direction.UP | Direction.DOWN)\
            || new_directions == (Direction.LEFT | Direction.RIGHT):
            return Straight.new(new_directions)
        else:
            return Corner.new(new_directions)
    elif direction_count == 3:
        return Junction.new(new_directions)
    else:
        return Cross.new()

static func remove_direction_from(conveyor: Base, direction: int) -> Object:
    var new_directions := conveyor.get_directions() & ~direction
    if new_directions == conveyor.get_directions():
        return conveyor

    var direction_count = Direction.count_directions(new_directions)

    if direction_count == 1:
        return Stop.new(new_directions)
    elif direction_count == 2:
        if new_directions == (Direction.UP | Direction.DOWN)\
            || new_directions == (Direction.LEFT | Direction.RIGHT):
            return Straight.new(new_directions)
        else:
            return Corner.new(new_directions)
    elif direction_count == 3:
        return Junction.new(new_directions)
    else:
        return null
