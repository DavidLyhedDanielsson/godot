extends Node

static func swap_and_remove(arr: Array, i: int) -> void:
    arr[i] = arr[arr.size() - 1]
    arr.pop_back()

static func collect(iter) -> Array:
    var arr := []
    for item in iter:
        arr.append(item)
    return arr

static func reverse(arr: Array) -> Array:
    arr.invert()
    return arr

static func last(iter):
    for item in iter:
        pass
    return iter.current()
