extends Node

signal button_toggled

func init(assetPath):
    var asset = load(assetPath).instance()
    var ViewportRoot = $CenterContainer/ViewportContainer/Viewport/Spatial
    ViewportRoot.add_child(asset)
    (ViewportRoot.get_node("Camera") as Camera).look_at_from_position(Vector3(-1.4, 2.0, 1.4), Vector3(0.0, 0.5, 0.0), Vector3(0.0, 1.0, 0.0))
    return self

func _on_Button_toggled(value):
    emit_signal("button_toggled", value)
