extends "res://addons/gut/test.gd"

const CT = preload("res://scripts/conveyor_type.gd").ConveyorType
const Position = preload("res://scripts/position.gd")
const CTH = preload("res://scripts/conveyor_type_helpers.gd")

class TestWorld:
    extends "res://addons/gut/test.gd"

    enum FilterType {
        AND, OR, NOT
    }

    enum FUZZ {
        PARENT, NEW, BOTH
    }

    func translate_character(character):
        var type
        match character:
            '→': type = CT.RIGHT
            '↴': type = CT.RIGHT_DOWN
            '⬏': type = CT.RIGHT_UP
            '←': type = CT.LEFT
            '⬐': type = CT.LEFT_DOWN
            '⮤': type = CT.LEFT_UP
            '↓': type = CT.DOWN
            '↳': type = CT.DOWN_RIGHT
            '↲': type = CT.DOWN_LEFT
            '↑': type = CT.UP
            '↱': type = CT.UP_RIGHT
            '↰': type = CT.UP_LEFT
            '✥': type = CT.CROSS
            '⭲': type = CT.RIGHT_STOP
            '⭰': type = CT.LEFT_STOP
            '⭳': type = CT.DOWN_STOP
            '⭱': type = CT.UP_STOP
            '↥': type = CT.JUNC_HORI_UP
            '↧': type = CT.JUNC_HORI_DOWN
            '↤': type = CT.JUNC_VERT_LEFT
            '↦': type = CT.JUNC_VERT_RIGHT
            '⇑': type = CT.UP_SPAWN
            '⇓': type = CT.DOWN_SPAWN
            '⇐': type = CT.LEFT_SPAWN
            '⇒': type = CT.RIGHT_SPAWN
            '⇧': type = CT.UP_GOAL
            '⇩': type = CT.DOWN_GOAL
            '⇦': type = CT.LEFT_GOAL
            '⇨': type = CT.RIGHT_GOAL
            _ : type = CT.NONE
        return type

    func dump_world(world):
        var world_string: = ""
        var size = world.map.get_size()
        for z in range(size[1] - 1, -1, -1):
            for x in range(size[0]):
                world_string += CTH.type_to_string(world.map.get_at(x, z)) + " "
            world_string += "\n"
        return world_string

    func trim_world(world):
        var world_lines = world.split('\n')
        for i in range(world_lines.size()):
            world_lines[i] = world_lines[i].strip_edges()
        return world_lines

    func get_world_size(trimmed_world):
        return Position.new(trimmed_world[0].length(), trimmed_world.size())

    func flip_world(world):
        var flipped_world = []
        for i in range(world.size()):
            flipped_world.append(world[world.size() - 1 - i])
        return flipped_world

    func find_build_position(trimmed_world):
        var world_size = get_world_size(trimmed_world)
        for z in range(world_size.y):
            for x in range(world_size.x):
                if trimmed_world[z][x] == '↯':
                    return Position.new(x, z)
        return null

    func get_parent_position(build_position, parent_direction):
        return Position.new(build_position.x, build_position.y).move_in_direction(parent_direction)

    # Returns the cartesian product between two lists.
    #
    # To make it possible to use this function to generate the product between
    # any number of lists, the lhs parameter can be an array of tuples. I.e. the
    # following is valid: `cartesian_product(cartesian_product(a, b), c)`
    #
    # @returns {Array<Array>} An array with all tuples from combining lhs and rhs
    func cartesian_product(lhs: Array, rhs: Array):
        var result = []
        if lhs.size() == 0:
            for element in rhs:
                result.append(element)
            return result
        if rhs.size() == 0:
            for element in lhs:
                result.append(element)
            return result

        for first in lhs:
            for second in rhs:
                if typeof(first) == TYPE_ARRAY:
                    var new_tuple = []
                    for element in first:
                        new_tuple.append(element)
                    new_tuple.append(second)
                    result.append(new_tuple)
                else:
                    result.append([first, second])
        return result

    # Generates scenarios based on the given rules and asserts that that the
    # state is correct after building a conveyor.
    #
    # Important note:
    # The following characters are NOT normal unicode B, 1, 2, 3, etc. and must
    # be copy-pasted to work:
    # ↯❶❷❸❹❺❻❼❽❾
    #
    # Can be used to test build_conveyor perhaps a bit too thoroughly.
    #
    # Example:
    # `generate_scenario(
    #      """∎∎∎
    #         ∎↯∎
    #         ❶↧❷
    #         ∎❸∎""",
    #      {
    #          parent_direction = Direction.DOWN,
    #          expected_conveyor = CT.UP_STOP,
    #          expected_parent = CT.CROSS,
    #          connection_type = CT.OR,
    #          1: { connection = Direction.RIGHT },
    #          2: { connection = Direction.LEFT },
    #          3: { connection = Direction.UP },
    #      })`
    #
    # Will generate 1728 scenarios where the ❶, ❷, and ❸ symbols will be
    # conveyor types conecting towards right, left, and up respectively. The
    # number of scenarios comes from the combination of all conveyors that
    # fulfil rule ❶ combined with all conveyors that fulfill rule ❷ combined
    # with all conveyors that fulfill rule ❸. The number can be verified by
    # multiplying together the conveyor types from each rule; in this case
    # 12*12*12.
    #
    # Rules are defined by any combination of the following properties:
    # incoming - the conveyor has this specific incoming direction
    # outgoing - the conveyor has this specific outgoing direction
    # connection - the conveyor has a connection in this direction, either
    #              incoming or outgoing
    #
    # FilterTypes can be used to control how bitwse combinations are handled.
    # - FilterType.AND means all of the directions must match
    # - FilterType.OR means any of the directions can match
    # - FilterType.NOT means the direction should not be matched
    # The following properties control filter types, and their default is:
    # - incoming_type: AND
    # - outgoing_type: AND
    # - connection_type: OR
    #
    # Fuzzing is a concept where all conveyor types that do NOT create
    # connections to either the parent or the newly built conveyor are added.
    # The `fuzz` property in the `info` parameter controls what to fuzz. This
    # type of testing is useful to make sure there are no erroneous connections
    # created.
    #
    # If there is already a tile placed underneath the `↯`, the `placed`
    # property can be used. For instance: `placed = CT.UP`
    #
    func generate_scenario(scenario, info):
        var world = flip_world(trim_world(scenario))
        var world_size = get_world_size(world)
        var build_at = find_build_position(world)
        var parent_at = get_parent_position(build_at, info.parent_direction)

        assert_not_null(parent_at)

        var rules = []
        for z in range(world_size.y):
            for x in range(world_size.x):
                var current_position = Position.new(x, z)
                if current_position.equals(build_at) || current_position.equals(parent_at):
                    continue

                var number_to_index = ['❶','❷','❸','❹','❺','❻','❼','❽','❾']

                var incoming_filter = null
                var outgoing_filter = null
                var connection_filter = null
                var incoming_type = info.incoming_type  if info.has('incoming_type') else FilterType.AND
                var outgoing_type = info.outgoing_type if info.has('outgoing_type') else FilterType.AND
                var connection_type = info.connection_type if info.has('connection_type') else FilterType.OR

                if number_to_index.has(world[z][x]):
                    var index = number_to_index.find(world[z][x]) + 1

                    incoming_filter = info[index].get('incoming')
                    outgoing_filter = info[index].get('outgoing')
                    connection_filter = info[index].get('connection')
                elif info.get("fuzz", null) != null && world[z][x] == '∎':
                    var fuzz = info.get("fuzz")
                    if (fuzz == FUZZ.PARENT && current_position.is_next_to(parent_at))\
                        || (fuzz == FUZZ.NEW && current_position.is_next_to(build_at))\
                        || (fuzz == FUZZ.BOTH && (current_position.is_next_to(parent_at) || current_position.is_next_to(build_at))):
                        var target = parent_at if current_position.is_next_to(parent_at) else build_at
                        var build_direction = current_position.get_direction_to(target)
                        connection_filter = build_direction
                        connection_type = FilterType.NOT
                    else:
                        continue
                else:
                    continue

                var new_rules = []
                for conveyor in CT:
                    var incoming = CTH.incoming_direction(CT[conveyor])
                    var outgoing = CTH.outgoing_direction(CT[conveyor])
                    if incoming_filter != null:
                        if incoming_type == FilterType.AND && incoming != incoming_filter:
                            continue
                        if incoming_type == FilterType.OR && (incoming & incoming_filter == 0):
                            continue
                        if incoming_type == FilterType.NOT && (incoming & incoming_filter != 0):
                            continue
                    if outgoing_filter != null:
                        if outgoing_type == FilterType.AND && outgoing != outgoing_filter:
                            continue
                        if outgoing_type == FilterType.OR && (outgoing & outgoing_filter == 0):
                            continue
                        if outgoing_type == FilterType.NOT && (outgoing & outgoing_filter != 0):
                            continue
                    if connection_filter != null:
                        if connection_type == FilterType.AND && incoming != CTH.flip_direction(connection_filter) && outgoing != connection_filter:
                            continue
                        if connection_type == FilterType.OR && (incoming & CTH.flip_direction(connection_filter) == 0) && (outgoing & connection_filter == 0):
                            continue
                        if connection_type == FilterType.NOT && ((incoming & CTH.flip_direction(connection_filter) != 0) || (outgoing & connection_filter != 0)):
                            continue
                    new_rules.append({position = Position.new(x, z), conveyor_type = CT[conveyor]})

                if !new_rules.empty():
                    rules.append(new_rules)

        var rule_combinations = []
        if rules.size()  == 1:
            for rule in rules[0]:
                rule_combinations.append([rule])
        else:
            for rule in rules:
                rule_combinations = cartesian_product(rule_combinations, rule)

        # If there are no rules i.e. no ❶, ❷, etc. in the scenario it should
        # still be ran through to verify that it is valid, so add an "fake" rule
        # to trigger it.
        if rule_combinations.empty():
            rule_combinations.append([])

        for scenario_rules in rule_combinations:
            var scenario_world = preload("res://scripts/world.gd").new(world_size.x, world_size.y)
            for z in range(world_size.y):
                for x in range(world_size.x):
                    if Position.new(x, z).equals(build_at) && info.get('placed') != null:
                        scenario_world.set_at(x, z, info.get('placed'))
                    else:
                        scenario_world.set_at(x, z, translate_character(world[z][x]))

            for scenario_rule in scenario_rules:
                var position = scenario_rule.position
                var conveyor_type = scenario_rule.conveyor_type
                scenario_world.set_at(position.x, position.y, conveyor_type)

            scenario_world.build_conveyor(parent_at.x, parent_at.y, build_at.x, build_at.y)

            assert_eq(scenario_world.get_at(build_at.x, build_at.y),
                      info.expected_conveyor,
                      "Expected %s at (%s, %s)" % [CTH.type_to_string(info.expected_conveyor), build_at.x, build_at.y] +\
                      "\n" + dump_world(scenario_world))
            assert_eq(scenario_world.get_at(parent_at.x, parent_at.y),
                      info.expected_parent,
                      "Expected %s at (%s, %s)" % [CTH.type_to_string(info.expected_parent), parent_at.x, parent_at.y] +\
                      "\n" + dump_world(scenario_world))

    func test_up():
        generate_scenario(
            """∎∎∎
               ∎↯∎
               ❶↧❷
               ∎❸∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.CROSS,
                1: { connection = Direction.RIGHT },
                2: { connection = Direction.LEFT },
                3: { connection = Direction.UP },
            })

        generate_scenario(
            """∎∎∎
               ∎↯∎
               ∎⭱∎
               ∎❶∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.UP,
                1: { outgoing = Direction.UP },
            })



        generate_scenario(
            """∎∎∎
               ∎↯∎
               ∎⭰❶""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.LEFT_UP,
                1: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎∎∎
               ∎↯∎
               ❶⭲∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.RIGHT_UP,
                1: { outgoing = Direction.RIGHT },
            })

        generate_scenario(
            """∎∎∎
               ∎↯∎
               ❶→❷""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.JUNC_HORI_UP,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.RIGHT },
            })

        generate_scenario(
            """∎∎∎
               ∎↯∎
               ❶←❷""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.JUNC_HORI_UP,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎∎∎
               ∎↯∎
               ∎⭱∎
               ∎↑∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.UP,
                fuzz = FUZZ.NEW,
            })

        generate_scenario(
            """∎∎∎
               ∎↯∎
               ∎⭱∎
               ∎↑∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_STOP,
                expected_parent = CT.UP,
                fuzz = FUZZ.PARENT,
            })

        # This scenario generates a grand total of 1419857 conveyor
        # combinations. Takes a while to run, so maybe don't
        # generate_scenario(
        #     """∎∎∎
        #        ∎↯∎
        #        ∎⭱∎
        #        ∎↑∎""",
        #     {
        #         parent_direction = Direction.DOWN,
        #         expected_conveyor = CT.UP_STOP,
        #         expected_parent = CT.UP,
        #         fuzz = FUZZ.BOTH
        #     })

    func test_down():
        generate_scenario(
            """∎❸∎
               ❶↥❷
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.CROSS,
                1: { connection = Direction.RIGHT },
                2: { connection = Direction.LEFT },
                3: { connection = Direction.DOWN },
            })

        generate_scenario(
            """∎❶∎
               ∎⭳∎
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.DOWN,
                1: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎⭰❶
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.LEFT_DOWN,
                1: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """❶⭲∎
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.RIGHT_DOWN,
                1: { outgoing = Direction.RIGHT },
            })

        generate_scenario(
            """❶→❷
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.JUNC_HORI_DOWN,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.RIGHT },
            })

        generate_scenario(
            """❶←❷
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.JUNC_HORI_DOWN,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎↓∎
               ∎⭳∎
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.DOWN,
                fuzz = FUZZ.NEW,
            })

        generate_scenario(
            """∎↓∎
               ∎⭳∎
               ∎↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_STOP,
                expected_parent = CT.DOWN,
                fuzz = FUZZ.PARENT,
            })

    func test_left():
        generate_scenario(
            """∎∎❸∎
               ∎↯↦❷
               ∎∎❶∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.CROSS,
                1: { connection = Direction.UP },
                2: { connection = Direction.LEFT },
                3: { connection = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎∎
               ∎↯⭰❶
               ∎∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.LEFT,
                1: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎∎❶
               ∎↯⭳
               ∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.DOWN_LEFT,
                1: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎
               ∎↯⭱
               ∎∎❶""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.UP_LEFT,
                1: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎∎❷
               ∎↯↑
               ∎∎❶""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.JUNC_VERT_LEFT,
                1: { outgoing = Direction.UP },
                2: { incoming = Direction.UP },
            })

        generate_scenario(
            """∎∎❷
               ∎↯↓
               ∎∎❶""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.JUNC_VERT_LEFT,
                1: { incoming = Direction.DOWN },
                2: { outgoing = Direction.DOWN },
            })


        generate_scenario(
            """∎∎∎∎
               ∎↯⭰←
               ∎∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.LEFT,
                fuzz = FUZZ.NEW,
            })

        generate_scenario(
            """∎∎∎∎
               ∎↯⭰←
               ∎∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_STOP,
                expected_parent = CT.LEFT,
                fuzz = FUZZ.PARENT,
            })

    func test_right():
        generate_scenario(
            """∎❸∎∎
               ❷↤↯∎
               ∎❶∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.CROSS,
                1: { connection = Direction.UP },
                2: { connection = Direction.RIGHT },
                3: { connection = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎∎
               ❶⭲↯∎
               ∎∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.RIGHT,
                1: { outgoing = Direction.RIGHT },
            })

        generate_scenario(
            """❶∎∎
               ⭳↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.DOWN_RIGHT,
                1: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎
               ⭱↯∎
               ❶∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.UP_RIGHT,
                1: { outgoing = Direction.UP },
            })

        generate_scenario(
            """❷∎∎
               ↑↯∎
               ❶∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.JUNC_VERT_RIGHT,
                1: { outgoing = Direction.UP },
                2: { incoming = Direction.UP },
            })

        generate_scenario(
            """❷∎∎
               ↓↯∎
               ❶∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.JUNC_VERT_RIGHT,
                1: { incoming = Direction.DOWN },
                2: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎∎
               →⭲↯∎
               ∎∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.RIGHT,
                fuzz = FUZZ.NEW
            })

        generate_scenario(
            """∎∎∎∎
               →⭲↯∎
               ∎∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_STOP,
                expected_parent = CT.RIGHT,
                fuzz = FUZZ.PARENT
            })

    func test_build_into_stop():
        generate_scenario(
            """∎∎∎
               ❶↯∎
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_LEFT,
                expected_parent = CT.UP,
                placed = CT.RIGHT_STOP,
                1: { outgoing = Direction.RIGHT },
            })

        generate_scenario(
            """∎❶∎
               ∎↯∎
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP,
                expected_parent = CT.UP,
                placed = CT.DOWN_STOP,
                1: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎
               ∎↯❶
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.UP_RIGHT,
                expected_parent = CT.UP,
                placed = CT.LEFT_STOP,
                1: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎❶∎
               ⭲↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_UP,
                expected_parent = CT.RIGHT,
                placed = CT.DOWN_STOP,
                1: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎
               ⭲↯❶
               ∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT,
                expected_parent = CT.RIGHT,
                placed = CT.LEFT_STOP,
                1: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎∎∎
               ⭲↯∎
               ∎❶∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.RIGHT_DOWN,
                expected_parent = CT.RIGHT,
                placed = CT.UP_STOP,
                1: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎⭳∎
               ∎↯❶
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_RIGHT,
                expected_parent = CT.DOWN,
                placed = CT.LEFT_STOP,
                1: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎⭳∎
               ∎↯∎
               ∎❶∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN,
                expected_parent = CT.DOWN,
                placed = CT.UP_STOP,
                1: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎⭳∎
               ❶↯∎
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.DOWN_LEFT,
                expected_parent = CT.DOWN,
                placed = CT.RIGHT_STOP,
                1: { outgoing = Direction.RIGHT },
            })

        generate_scenario(
            """∎∎∎
               ∎↯⭰
               ∎❶∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_DOWN,
                expected_parent = CT.LEFT,
                placed = CT.UP_STOP,
                1: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎∎∎
               ❶↯⭰
               ∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT,
                expected_parent = CT.LEFT,
                placed = CT.RIGHT_STOP,
                1: { outgoing = Direction.RIGHT },
            })

        generate_scenario(
            """∎❶∎
               ∎↯⭰
               ∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.LEFT_UP,
                expected_parent = CT.LEFT,
                placed = CT.DOWN_STOP,
                1: { outgoing = Direction.DOWN },
            })

    func test_build_up():
        generate_scenario(
            """∎∎∎
               ❶↯❷
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.JUNC_HORI_DOWN,
                expected_parent = CT.UP,
                placed = CT.LEFT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎∎∎
               ❶↯❷
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.JUNC_HORI_DOWN,
                expected_parent = CT.UP,
                placed = CT.RIGHT,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.RIGHT },
            })

        generate_scenario(
            """∎❷∎
               ❶↯∎
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.JUNC_VERT_LEFT,
                expected_parent = CT.UP,
                placed = CT.RIGHT_UP,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.UP },
            })

        generate_scenario(
            """∎❷∎
               ❶↯∎
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.JUNC_VERT_LEFT,
                expected_parent = CT.UP,
                placed = CT.DOWN_LEFT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎❷∎
               ∎↯❶
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.JUNC_VERT_RIGHT,
                expected_parent = CT.UP,
                placed = CT.LEFT_UP,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.LEFT },
                2: { incoming = Direction.UP },
            })

        generate_scenario(
            """∎❷∎
               ∎↯❶
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.JUNC_VERT_RIGHT,
                expected_parent = CT.UP,
                placed = CT.DOWN_RIGHT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.RIGHT },
                2: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎❸∎
               ❶↯❷
               ∎⭱∎""",
            {
                parent_direction = Direction.DOWN,
                expected_conveyor = CT.CROSS,
                expected_parent = CT.UP,
                placed = CT.JUNC_HORI_UP,
                1: { connection = Direction.RIGHT },
                2: { connection = Direction.LEFT },
                3: { connection = Direction.DOWN },
            })

    func test_build_down():
        generate_scenario(
            """∎⭳∎
               ❶↯❷
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.JUNC_HORI_UP,
                expected_parent = CT.DOWN,
                placed = CT.LEFT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎⭳∎
               ❶↯❷
               ∎∎∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.JUNC_HORI_UP,
                expected_parent = CT.DOWN,
                placed = CT.RIGHT,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.RIGHT },
            })

        generate_scenario(
            """∎⭳∎
               ❶↯∎
               ∎❷∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.JUNC_VERT_LEFT,
                expected_parent = CT.DOWN,
                placed = CT.RIGHT_DOWN,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.DOWN },
            })

        generate_scenario(
            """∎⭳∎
               ❶↯∎
               ∎❷∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.JUNC_VERT_LEFT,
                expected_parent = CT.DOWN,
                placed = CT.UP_LEFT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎⭳∎
               ∎↯❶
               ∎❷∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.JUNC_VERT_RIGHT,
                expected_parent = CT.DOWN,
                placed = CT.LEFT_DOWN,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.LEFT },
                2: { incoming = Direction.DOWN },
            })

        generate_scenario(
            """∎⭳∎
               ∎↯❶
               ∎❷∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.JUNC_VERT_RIGHT,
                expected_parent = CT.DOWN,
                placed = CT.UP_RIGHT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.RIGHT },
                2: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎⭳∎
               ❶↯❷
               ∎❸∎""",
            {
                parent_direction = Direction.UP,
                expected_conveyor = CT.CROSS,
                expected_parent = CT.DOWN,
                placed = CT.JUNC_HORI_DOWN,
                1: { connection = Direction.RIGHT },
                2: { connection = Direction.LEFT },
                3: { connection = Direction.UP },
            })

    func test_build_left():
        generate_scenario(
            """∎❶∎
               ∎↯⭰
               ∎❷∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.JUNC_VERT_RIGHT,
                expected_parent = CT.LEFT,
                placed = CT.UP,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.UP },
                2: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎❶∎
               ∎↯⭰
               ∎❷∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.JUNC_VERT_RIGHT,
                expected_parent = CT.LEFT,
                placed = CT.DOWN,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.DOWN },
                2: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎❷∎
               ❶↯⭰
               ∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.JUNC_HORI_UP,
                expected_parent = CT.LEFT,
                placed = CT.RIGHT_UP,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.UP },
            })

        generate_scenario(
            """∎❷∎
               ❶↯⭰
               ∎∎∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.JUNC_HORI_UP,
                expected_parent = CT.LEFT,
                placed = CT.DOWN_LEFT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎
               ❶↯⭰
               ∎❷∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.JUNC_HORI_DOWN,
                expected_parent = CT.LEFT,
                placed = CT.RIGHT_DOWN,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.RIGHT },
                2: { incoming = Direction.DOWN },
            })

        generate_scenario(
            """∎∎∎
               ❶↯⭰
               ∎❷∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.JUNC_HORI_DOWN,
                expected_parent = CT.LEFT,
                placed = CT.UP_LEFT,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.LEFT },
                2: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎❸∎
               ❶↯⭰
               ∎❷∎""",
            {
                parent_direction = Direction.RIGHT,
                expected_conveyor = CT.CROSS,
                expected_parent = CT.LEFT,
                placed = CT.JUNC_VERT_LEFT,
                1: { connection = Direction.RIGHT },
                2: { connection = Direction.UP },
                3: { connection = Direction.DOWN },
            })

    func test_build_right():
        generate_scenario(
            """∎❶∎
               ⭲↯∎
               ∎❷∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.JUNC_VERT_LEFT,
                expected_parent = CT.RIGHT,
                placed = CT.UP,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.UP },
                2: { outgoing = Direction.UP },
            })

        generate_scenario(
            """∎❶∎
               ⭲↯∎
               ∎❷∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.JUNC_VERT_LEFT,
                expected_parent = CT.RIGHT,
                placed = CT.DOWN,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.DOWN },
                2: { outgoing = Direction.DOWN },
            })

        generate_scenario(
            """∎❶∎
               ⭲↯❷
               ∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.JUNC_HORI_UP,
                expected_parent = CT.RIGHT,
                placed = CT.DOWN_RIGHT,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.DOWN },
                2: { incoming = Direction.RIGHT },
            })

        generate_scenario(
            """∎❶∎
               ⭲↯❷
               ∎∎∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.JUNC_HORI_UP,
                expected_parent = CT.RIGHT,
                placed = CT.LEFT_UP,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.UP },
                2: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎∎∎
               ⭲↯❷
               ∎❶∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.JUNC_HORI_DOWN,
                expected_parent = CT.RIGHT,
                placed = CT.UP_RIGHT,
                fuzz = FUZZ.NEW,
                1: { outgoing = Direction.UP },
                2: { incoming = Direction.RIGHT },
            })

        generate_scenario(
            """∎∎∎
               ⭲↯❷
               ∎❶∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.JUNC_HORI_DOWN,
                expected_parent = CT.RIGHT,
                placed = CT.LEFT_DOWN,
                fuzz = FUZZ.NEW,
                1: { incoming = Direction.DOWN },
                2: { outgoing = Direction.LEFT },
            })

        generate_scenario(
            """∎❸∎
               ⭲↯❶
               ∎❷∎""",
            {
                parent_direction = Direction.LEFT,
                expected_conveyor = CT.CROSS,
                expected_parent = CT.RIGHT,
                placed = CT.JUNC_VERT_RIGHT,
                1: { connection = Direction.RIGHT },
                2: { connection = Direction.UP },
                3: { connection = Direction.DOWN },
            })
