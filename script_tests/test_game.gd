extends "res://addons/gut/test.gd"

const TimeConstants = preload("res://scripts/time_constants.gd")

const Router = preload("res://scripts/router.gd")
const Position = preload("res://scripts/position.gd")

const Straight = preload("res://scripts/conveyors/straight.gd")
const Corner = preload("res://scripts/conveyors/corner.gd")
const Goal = preload("res://scripts/conveyors/goal.gd")
const Stop = preload("res://scripts/conveyors/stop.gd")
const Cross = preload("res://scripts/conveyors/cross.gd")
const Junction = preload("res://scripts/conveyors/junction.gd")
const Spawner = preload("res://scripts/conveyors/spawner.gd")

class Game:
    const World = preload("res://scripts/world.gd")

    var world
    var router
    func _init():
        pass
    func world_size_x():
        return world.map.size_x()
    func world_size_z():
        return world.map.size_z()
    func create_new_scenario(size_x, size_z):
        self.world = World.new(size_x, size_z)
        self.router = Router.new(size_x, size_z)
        pass
    func update_conveyors():
        pass

class TestGame:
    extends "res://addons/gut/test.gd"

    const World = preload("res://scripts/world.gd")

    func simulate_movement(world, router, steps, expected_cargo_count):
        assert_eq(TimeConstants.CARGO_STEPS % TimeConstants.CARGO_STEPS_PER_TICK, 0, "CARGO_STEPS not divisible by CARGO_STEPS_PER_TICK")
        # 1 spawn = moving from center to edge
        # 1 goal = moving from edge to center
        for _i in range(TimeConstants.CARGO_STEPS * (2 + steps) / TimeConstants.CARGO_STEPS_PER_TICK - 1):
            world.update_conveyors(router)
            assert_eq(world.cargoes.size(), expected_cargo_count, "Unexpected cargo count during simulation")

    func expect_cargo_at(world, router, x, z, expected_total):
        assert_eq(expected_total, world.cargoes.size(), "Unexpected cargo count after simulation")
        var found_cargo
        for cargo in world.cargoes:
            #var cargo = world.cargoes[0]
            if cargo.position.x == x && cargo.position.y == z:
                found_cargo = cargo
                break

        assert_not_null(found_cargo, "Could not find cargo at %s, %s" % [x, z])

    const FileUtil = preload("res://scripts/file_util.gd")

    func load_simple_test_file(file_name, steps):
        var fake_game = Game.new()
        FileUtil.load_test_scenario("res://script_tests/test_game_scenarios/" + file_name + ".txt", fake_game)

        var spawners = []
        var goals = []

        for z in range(fake_game.world.map.size_z()):
            for x in range(fake_game.world.map.size_x()):
                var conveyor = fake_game.world.map.get_at_xz(x, z)
                if conveyor != null:
                    if conveyor.is_spawner():
                        spawners.append(Position.new(x, z))
                    elif conveyor.is_goal():
                        goals.append(Position.new(x, z))

        for position in spawners:
            fake_game.world.force_spawn(fake_game.router, position.x, position.y)
        simulate_movement(fake_game.world, fake_game.router, steps, spawners.size())
        for position in goals:
            expect_cargo_at(fake_game.world, fake_game.router, position.x, position.y, spawners.size())

    func load_test_scenario(file_name):
        var fake_game = Game.new()
        FileUtil.load_test_scenario("res://script_tests/test_game_scenarios/" + file_name + ".txt", fake_game)

        var events = {}

        var file = File.new()
        file.open("res://script_tests/test_game_scenarios/" + file_name + ".txt", File.READ)
        var skips = 2
        while !file.eof_reached():
            var line = file.get_line()
            if skips == 0:
                var event_data = line.split(' ')
                var tick = int(event_data[0])
                var action = event_data[1]
                var data
                if event_data.size() == 3:
                    data = event_data[2]

                var new_event = {'tick': tick, 'action': action, 'data': data}
                if events.has(tick):
                    events[tick].push_back(new_event)
                else:
                    events[tick] = [new_event]
            elif line == '':
                skips -= 1

        var run = true
        var tick = 0
        var tick_step = 0
        var cargo_count = 0
        while(run && tick < 10000):
            if events.has(tick_step):
                gut.p("Tick step %s (%s):" % [tick_step, tick])
                var event_list = events[tick_step]
                for event in event_list:
                    match event['action']:
                        'spawn':
                            gut.p("    Spawning")
                            var position = fake_game.router.decode_id(int(event.data))
                            fake_game.world.force_spawn(fake_game.router, position.x, position.y)
                            cargo_count += 1
                            gut.p("        Cargo count: %s" % cargo_count)
                        'goal':
                            gut.p("    Goal")
                            cargo_count -= 0
                            gut.p("        Cargo count: %s" % cargo_count)
                        'end':
                            gut.p("    End")
                            assert_eq(fake_game.world.cargoes.size(), int(event.data), "Unexpected cargo count at end")
                            run = false
            if tick % int(TimeConstants.CARGO_STEPS * 2.0 / TimeConstants.CARGO_STEPS_PER_TICK) == 0:
                tick_step += 1
            fake_game.world.update_conveyors(fake_game.router)
            if tick == 10000:
                assert_true(false, "Test exceeded tick timeout of 10000")
            tick += 1


    var simple_test_cases = [
        { "name": "corners", "steps": 12},
        { "name": "horizontal_cross", "steps": 6},
        #{ "name": "horizontal_double_junction_after_spawn", "steps": 6},
        { "name": "horizontal_double_junction_before_goal", "steps": 6},
        { "name": "horizontal_junc_down", "steps": 6},
        { "name": "horizontal_junc_up", "steps": 6},
        { "name": "horizontal_trivial", "steps": 2},
        { "name": "horizontal_trivial_m", "steps": 2},
        { "name": "vertical_cross", "steps": 6},
        { "name": "vertical_junc_left", "steps": 6},
        { "name": "vertical_junc_right", "steps": 6},
        { "name": "vertical_trivial", "steps": 2},
        { "name": "vertical_trivial_m", "steps": 2},
        { "name": "cross_double_track", "steps": 8},
    ]

    func test_simple_cases(params=use_parameters(simple_test_cases)):
        load_simple_test_file(params["name"], params["steps"])

    var test_scenario_names = [
        { "name": "junction_double_track" }
    ]

    func test_scenarios(params=use_parameters(test_scenario_names)):
        load_test_scenario(params["name"])
